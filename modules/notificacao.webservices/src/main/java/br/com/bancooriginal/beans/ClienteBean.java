package br.com.bancooriginal.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlType(name= "Cliente")
public class ClienteBean extends PessoaBean {
	
	@XmlElement(name= "idCliente" ,required=true)
	private Integer idCliente;
}
