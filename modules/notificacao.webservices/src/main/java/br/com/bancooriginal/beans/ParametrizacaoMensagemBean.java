package br.com.bancooriginal.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name= "ParametrizacaoMensagem")
public class ParametrizacaoMensagemBean implements Serializable {
	
	@XmlElement(name= "codigoMensagem" ,required=true)
	Integer codigoMensagem;
	@XmlElement(name= "codigoCanal" ,required=true)
	Integer codigoCanal;

}
