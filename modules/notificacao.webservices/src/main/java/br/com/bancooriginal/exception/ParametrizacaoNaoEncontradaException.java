package br.com.bancooriginal.exception;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.ws.WebFault;

@WebFault(name="ParametrizacaoNaoEncontrada")
@XmlAccessorType( XmlAccessType.FIELD )
public class ParametrizacaoNaoEncontradaException extends RuntimeException {
}
