package br.com.bancooriginal.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="Transacao")
public class TransacaoBean implements Serializable {

	@XmlElement(name= "idTransacao" ,required=true)
	private Integer idTransacao;
	@XmlElement(name= "idRazaoTransacao" ,required=true)
	private Integer idRazaoTransacao;
	@XmlElement(name= "valorTransacao" ,required=true)
	private Double valorTransacao;
}
