package br.com.bancooriginal.exception;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.ws.WebFault;

@WebFault(name="ConfiguracaoNotificacao")
@XmlAccessorType( XmlAccessType.FIELD )
public class ConfiguracaoNotificacaoException extends RuntimeException {

}
