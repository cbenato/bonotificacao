package br.com.bancooriginal.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name= "ParametroModeloMensagem")
public class ParametroModeloMensagemBean implements Serializable {

	@XmlElement(name= "idParametroModeloMensagem" ,required=true)
	private String idParametroModeloMensagem;
	@XmlElement(name= "valorParametroModeloMensagem" ,required=true)
	private String valorParametroModeloMensagem;
	
}
