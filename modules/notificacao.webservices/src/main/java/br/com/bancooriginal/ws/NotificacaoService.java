package br.com.bancooriginal.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import br.com.bancooriginal.beans.NotificacaoBean;
import br.com.bancooriginal.beans.NotificacaoRetornoBean;
import br.com.bancooriginal.exception.ClienteNaoEncontradoException;
import br.com.bancooriginal.exception.FuncionarioNaoEncontradoException;
import br.com.bancooriginal.exception.ParametrizacaoNaoEncontradaException;

@WebService
public interface NotificacaoService {

	@WebMethod
	@WebResult(name = "NotificacaoRetorno")
	public NotificacaoRetornoBean solicitarNotificacao(@XmlElement(required=true) @WebParam(name="notificacao") NotificacaoBean notificacao) 
			throws ClienteNaoEncontradoException,
			FuncionarioNaoEncontradoException,
			ParametrizacaoNaoEncontradaException;
	
}
