package br.com.bancooriginal.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name= "filtroHistoricoNotificacao")
public class FiltroHistoricoNotificacaoBean implements Serializable {
	
	@XmlElement(name= "idCliente" ,required=true)
	private Integer idCliente;
	@XmlElement(name= "chaveProduto" ,required=false)
	private String chaveProduto;
	@XmlElement(name= "dataEnvioInicio" ,required=true)
	private Date dataEnvioInicio;
	@XmlElement(name= "dataEnvioFim" ,required=true)
	private Date dataEnvioFim;
	@XmlElement(name= "codigoCanalMensagem" ,required=false)
	private Integer codigoCanalMensagem;
	@XmlElement(name= "codigoStatusEnvio" ,required=false)
	private Integer codigoStatusEnvio;
	@XmlElement(name= "codigoProduto" ,required=false)
	private Integer codigoProduto;
	@XmlElement(name= "codigoGrupo" ,required=false)
	private Integer codigoGrupo;
	@XmlElement(name= "codigoSubGrupo" ,required=false)
	private Integer codigoSubGrupo;
	@XmlElement(name= "idEvento" ,required=false)
	private Integer idEvento;
	@XmlElement(name= "idTransacao" ,required=false)
	private Integer idTransacao;	
	@XmlElement(name= "idNotificacao" ,required=false)
	private Integer idNotificacao;
	@XmlElement(name= "configuravelPeloCliente" ,required=false)
	private Boolean configuravelPeloCliente;
	
}
