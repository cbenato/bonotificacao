package br.com.bancooriginal.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="NotificacaoRetorno")
@XmlType(name= "NotificacaoRetorno")
public class NotificacaoRetornoBean implements Serializable {

	@XmlElement(name= "mensagem" ,required=true)
	private String mensagem;
	
	@XmlElement(name= "codigo" ,required=true)
	private Integer codigo;

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	
	
}
