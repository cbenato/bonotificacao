package br.com.bancooriginal.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConfiguracaoNotificacaoRetorno")
public class ConfiguracaoNotificacaoRetornoBean implements Serializable {

	@XmlElement(name= "mensagem" ,required=true)
	private String mensagem;
	@XmlElement(name= "codigo" ,required=true)
	private String codigo;
	
}
