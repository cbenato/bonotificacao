package br.com.bancooriginal.beans;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name= "parametrizacaoNotificacao")
public class ParametrizacaoNotificacaoBean implements Serializable {

	@XmlElement(name= "idEvento" ,required=true)
	Integer idEvento;
	@XmlElement(name= "codigoProduto" ,required=true)
	Integer codigoProduto;
	@XmlElement(name= "codigoGrupo" ,required=true)
	Integer codigoGrupo;
	@XmlElement(name= "codigoSubGrupo" ,required=true)
	Integer codigoSubGrupo;
	@XmlElement(name= "numeroNivel" ,required=true)
	Integer numeroNivel;
	@XmlElement(name= "idAlerta" ,required=true)
	Integer idAlerta;
	@XmlElement(name= "nomeAlerta" ,required=true)
	String nomeAlerta;
	@XmlElement(name= "configuravelPeloCliente" ,required=true)
	Boolean configuravelPeloCliente;
	@XmlElement(name= "avancado" ,required=true)
	Boolean avancado;
	@XmlElement(name= "defaultLigado" ,required=true)
	Boolean defaultLigado;
	@XmlElement(name= "permitePeriodicidade" ,required=true)
	Boolean permitePeriodicidade;
	@XmlElement(name= "permiteValorMinimo" ,required=true)
	Boolean permiteValorMinimo;
	@XmlElement(name= "ativo" ,required=true)
	Boolean ativo;

	
	@XmlElement(name= "parametrizacaoMensagem" ,required=true)
	List<ParametrizacaoMensagemBean> parametrizacaoMensagem;
	
	
}
