package br.com.bancooriginal.ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import br.com.bancooriginal.beans.ConfiguracaoNotificacaoBean;
import br.com.bancooriginal.beans.ConfiguracaoNotificacaoRetornoBean;

@WebService
public interface ConfiguracaoNotificacaoService {

	
	@WebMethod
	@WebResult(name = "ConfiguracaoNotificacaoRetorno")
	public ConfiguracaoNotificacaoRetornoBean incluirConfiguracao(@XmlElement(required=true) @WebParam(name="configuracaoNotificacao") ConfiguracaoNotificacaoBean configuracaoNotificacao);
	
	
	@WebMethod
	@WebResult(name = "ConfiguracaoNotificacaoRetorno")
	public ConfiguracaoNotificacaoRetornoBean alterarConfiguracao(@XmlElement(required=true) @WebParam(name="configuracaoNotificacao") ConfiguracaoNotificacaoBean configuracaoNotificacao);
	
	
	@WebMethod
	@WebResult(name = "ListaConfiguracaoNotificacao")
	public List<ConfiguracaoNotificacaoBean> consultarConfiguracao(
			@XmlElement(name= "idCliente" ,required=true) @WebParam(name="idCliente") 
			Integer idCliente,
			@XmlElement(name= "codProduto" ,required=false) @WebParam(name="codProduto") 
			Integer codigoProduto,
			@XmlElement(name= "chaveProduto" ,required=false) @WebParam(name="chaveProduto") 
			String chaveProduto,
			@XmlElement(name= "idResponsavel" ,required=false) @WebParam(name="idResponsavel") 
			String idResponsavel,
			@XmlElement(name= "idEvento" ,required=false) @WebParam(name="idEvento") 
			Integer idEvento,
			@XmlElement(name= "idTransacao" ,required=false) @WebParam(name="idTransacao") 
			Integer idTransacao,			
			@XmlElement(name= "configuravelPeloCliente" ,required=false) @WebParam(name="configuravelPeloCliente") 
			Boolean configuravelPeloCliente);

}
