package br.com.bancooriginal.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name= "configuracaoNotificacao")
public class ConfiguracaoNotificacaoBean implements Serializable {

	@XmlElement(name= "idCliente" ,required=true)
	Integer idCliente;
	@XmlElement(name= "codigoProduto" ,required=true)
	Integer codigoProduto;
	@XmlElement(name= "chaveProduto" ,required=true)
	String chaveProduto;
	@XmlElement(name= "idResponsavel" ,required=true)
	Integer idResponsavel;
	@XmlElement(name= "idEvento" ,required=false)
	Integer idEvento;
	@XmlElement(name= "idTransacao" ,required=true)
	Integer idTransacao;	
	@XmlElement(name= "codigoPeriodicidade" ,required=false)
	Integer codigoPeriodicidade;
	@XmlElement(name= "codigoPeriodo" ,required=false)
	Integer codigoPeriodo;
	@XmlElement(name= "valorMinimo" ,required=false)
	Double valorMinimo;
	@XmlElement(name= "alertaLigado" ,required=true)
	Boolean alertaLigado;
	
}
