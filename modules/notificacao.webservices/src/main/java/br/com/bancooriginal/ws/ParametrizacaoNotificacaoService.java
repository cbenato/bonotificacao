package br.com.bancooriginal.ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import br.com.bancooriginal.beans.ParametrizacaoNotificacaoBean;
import br.com.bancooriginal.beans.ParametrizacaoNotificacaoRetornoBean;

@WebService
public interface ParametrizacaoNotificacaoService {

	@WebMethod
	@WebResult(name = "ParametrizacaoNotificacaoRetorno")
	public ParametrizacaoNotificacaoRetornoBean incluirAlerta(@XmlElement(required=true) @WebParam(name="parametrizacaoNotificacao") ParametrizacaoNotificacaoBean parametrizacaoNotificacao);
	
	
	@WebMethod
	@WebResult(name = "ParametrizacaoNotificacaoRetorno")
	public ParametrizacaoNotificacaoRetornoBean alterarAlerta(@XmlElement(required=true) @WebParam(name="parametrizacaoNotificacao") ParametrizacaoNotificacaoBean parametrizacaoNotificacao);
	
	
	@WebMethod
	@WebResult(name = "ListaParametrizacaoNotificacao")
	public List<ParametrizacaoNotificacaoBean> consultarAlerta(
			@WebParam(name="idEvento") @XmlElement(name= "idEvento" ,required=false)
			Integer idEvento,
			@WebParam(name="idTransacao") @XmlElement(name= "idTransacao" ,required=false)
			Integer idTransacao,			
			@WebParam(name="codigoProduto") @XmlElement(name= "codigoProduto" ,required=false)
			Integer codigoProduto,			
			@WebParam(name="codigoGrupo") @XmlElement(name= "codigoGrupo" ,required=false)
			Integer codigoGrupo,
			@WebParam(name="codigoSubGrupo") @XmlElement(name= "codigoSubGrupo" ,required=false)
			Integer codigoSubGrupo,
			@WebParam(name="idAlerta") @XmlElement(name= "idAlerta" ,required=false)
			Integer idAlerta);
	
}
