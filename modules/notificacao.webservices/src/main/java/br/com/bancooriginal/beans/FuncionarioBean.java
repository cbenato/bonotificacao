package br.com.bancooriginal.beans;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name= "Funcionario")
public class FuncionarioBean extends PessoaBean {

	@XmlElement(name= "idFuncionario" ,required=true)
	private Integer idFuncionario;
	
}
