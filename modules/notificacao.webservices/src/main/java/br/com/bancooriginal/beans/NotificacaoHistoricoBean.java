package br.com.bancooriginal.beans;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name= "notificacaoHistorico")
@XmlType(name="NotificacaoHistorico")
public class NotificacaoHistoricoBean implements Serializable {
	
	@XmlElement(name= "dataHoraEnvio" ,required=true)
	private Date dataHoraEnvio;
	@XmlElement(name= "descCanalMensagem" ,required=true)
	private String descCanalMensagem;
	@XmlElement(name= "codigoCanalMensagem" ,required=true)
	private Integer codigoCanalMensagem;
	@XmlElement(name= "descStatusEnvio" ,required=true)
	private String descStatusEnvio;
	@XmlElement(name= "descProduto" ,required=true)
	private String descProduto;
	@XmlElement(name= "codigoProduto" ,required=true)
	private Integer codigoProduto;	
	@XmlElement(name= "descSubGrupo" ,required=true)
	private String descSubGrupo;
	@XmlElement(name= "codigoSubGrupo" ,required=true)
	private Integer codigoSubGrupo;
	@XmlElement(name= "descGrupo" ,required=true)
	private String descGrupo;
	@XmlElement(name= "codigoGrupo" ,required=true)
	private Integer codigoGrupo;		
	@XmlElement(name= "idEvento" ,required=false)
	private Integer idEvento;
	@XmlElement(name= "nomeEvento" ,required=false)
	private String nomeEvento;	
	@XmlElement(name= "idTransacao" ,required=false)
	private String idTransacao;	
	@XmlElement(name= "idNotificacao" ,required=true)
	private String idNotificacao;
	@XmlElement(name= "nomeNotificacao" ,required=true)
	private String nomeNotificacao;
	@XmlElement(name= "configuravelPeloCliente" ,required=true)
	private Boolean configuravelPeloCliente;
	@XmlElement(name= "conteudoMensagem" ,required=true)
	private String conteudoMensagem;
	

}
