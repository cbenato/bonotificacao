package br.com.bancooriginal.ws.impl;

import br.com.bancooriginal.beans.NotificacaoBean;
import br.com.bancooriginal.beans.NotificacaoRetornoBean;
import br.com.bancooriginal.ws.NotificacaoService;

public class NotificacaoServiceImpl implements NotificacaoService {

	@Override
	public NotificacaoRetornoBean solicitarNotificacao(
			NotificacaoBean notificacao) {
		NotificacaoRetornoBean retorno = new NotificacaoRetornoBean();
		retorno.setMensagem("Notificacao aceita e aguardando envio.");
		retorno.setCodigo(0);
		return retorno;
	}

}
