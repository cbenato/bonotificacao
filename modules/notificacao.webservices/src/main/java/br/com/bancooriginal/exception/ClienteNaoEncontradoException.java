package br.com.bancooriginal.exception;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.ws.WebFault;

@WebFault(name="ClienteNaoEncontrado")
@XmlAccessorType( XmlAccessType.FIELD )
public class ClienteNaoEncontradoException extends RuntimeException {


}
