package br.com.bancooriginal.beans;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement(name="Notificacao", namespace = "br.com.bancooriginal.notificacao.ws")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="Notificacao")
public class NotificacaoBean implements Serializable {
	

	@XmlElement(name= "idSistema" ,required=true)
	private String idSistema;
	@XmlElement(name= "codProduto" ,required=true)
	private Integer codProduto;
	@XmlElement(name= "chaveProduto" ,required=true)
	private String chaveProduto;
	@XmlElement(name= "email" ,required=false)
	private String email;
	@XmlElement(name= "telefone" ,required=false)
	private String telefone;	
	@XmlElements(value = { 
            @XmlElement(name="evento", 
                        type=EventoBean.class, required=true),
            @XmlElement(name="transacao", 
                        type=TransacaoBean.class, required=true),
      })
	private Object fatoDeDisparo;


	@XmlElement(name= "dataAgendamentoEnvio" ,required=false)
	private String dataAgendamentoEnvio;
	@XmlElement(name= "horaAgendamentoEnvio" ,required=false)
	private String horaAgendamentoEnvio;
		
	@XmlElements(value = { 
            @XmlElement(name="idCliente", 
                        type=ClienteBean.class, required=true),
            @XmlElement(name="idFuncionario", 
                        type=FuncionarioBean.class, required=true),
      })
	private PessoaBean pessoa;
	
	@XmlElement(name= "parametroModeloMensagem" ,required=true)
	private List<ParametroModeloMensagemBean> parametroModeloMensagem;
	

	
	

}
