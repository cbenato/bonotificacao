package br.com.bancooriginal.ws;

import java.io.Serializable;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import br.com.bancooriginal.beans.FiltroHistoricoNotificacaoBean;
import br.com.bancooriginal.beans.NotificacaoHistoricoBean;
import br.com.bancooriginal.exception.ClienteNaoEncontradoException;

@WebService
public interface HistoricoNotificacaoService extends Serializable {

	@WebMethod
	@WebResult(name = "ListaNotificacaoHistorico")
	public List<NotificacaoHistoricoBean> consultarHistorico(@XmlElement(required=true) @WebParam(name="filtro") FiltroHistoricoNotificacaoBean filtro) throws ClienteNaoEncontradoException;
}
