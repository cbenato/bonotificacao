package br.com.bancooriginal.notificacao.integration.ws.sms;

public interface TWWSMSResponseTransformer {

	public String formatXml(String result) throws Exception;
}
