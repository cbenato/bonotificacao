package br.com.bancooriginal.notificacao.integration.ws.sms;

import br.com.bancooriginal.notificacao.dto.SolicitacaoNotificacaoDTO;

public interface TWWSMSRequestTransformer {

	public String transform(SolicitacaoNotificacaoDTO  solicitacaoNotificacao);
	
}
