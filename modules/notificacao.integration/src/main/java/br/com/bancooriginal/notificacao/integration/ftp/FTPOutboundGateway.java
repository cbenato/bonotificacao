package br.com.bancooriginal.notificacao.integration.ftp;

import java.util.List;

public interface FTPOutboundGateway {
	
	public List<Boolean>  baixarArquivos(String diretorio);

}
