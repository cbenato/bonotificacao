package br.com.bancooriginal.notificacao.integration.ws.sms.transformer;

import java.io.StringWriter;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.springframework.xml.transform.StringSource;

import br.com.bancooriginal.notificacao.integration.ws.sms.TWWSMSResponseTransformer;

public class TWWSMSResponseTransformerImpl implements TWWSMSResponseTransformer {

	
	
	
	public static String formatResponseXml(String result) throws Exception {
		
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", String.valueOf(2));
		StreamResult streamResult = new StreamResult(new StringWriter());
		Source source = new StringSource(result);
		transformer.transform(source, streamResult);
		String xmlString = streamResult.getWriter().toString();
		return xmlString;
		
	}

	@Override
	public String formatXml(String result) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
