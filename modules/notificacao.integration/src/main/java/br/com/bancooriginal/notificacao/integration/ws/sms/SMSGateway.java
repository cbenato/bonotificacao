package br.com.bancooriginal.notificacao.integration.ws.sms;

import br.com.bancooriginal.notificacao.dto.SolicitacaoNotificacaoDTO;

public interface SMSGateway {
	
	public String enviarSMS(SolicitacaoNotificacaoDTO solicitacaoNotificacao);

}
