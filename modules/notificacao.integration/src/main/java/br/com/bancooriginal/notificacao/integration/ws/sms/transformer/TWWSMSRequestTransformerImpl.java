package br.com.bancooriginal.notificacao.integration.ws.sms.transformer;

import br.com.bancooriginal.notificacao.dto.SolicitacaoNotificacaoDTO;
import br.com.bancooriginal.notificacao.integration.ws.sms.TWWSMSRequestTransformer;

public class TWWSMSRequestTransformerImpl implements TWWSMSRequestTransformer {

	@Override
	public String transform(SolicitacaoNotificacaoDTO solicitacaoNotificacao) {
		
		String requestStr = "<ws:solicitarNotificacao xmlns:ws=\"http://ws.bancooriginal.com.br/\">" +
				"<notificacao>"+
				"	<idSistema>CNTA</idSistema>"+
				"	<codProduto>45</codProduto>"+
				"	<chaveProduto>4343434343</chaveProduto>"+
				"	<!--Optional:-->"+
				"	<email>teste@teste.com.br</email>"+
				"	<!--Optional:-->"+
				"	<telefone>5511984906666</telefone>"+
				"	<!--You have a CHOICE of the next 2 items at this level-->"+
				"	<evento>"+
				"	   <idEvento>2</idEvento>"+
				"	</evento>"+
				"	<transacao>"+
				"	   <idTransacao>5</idTransacao>"+
				"	   <idRazaoTransacao>6</idRazaoTransacao>"+
				"	   <valorTransacao>1000000000.00</valorTransacao>"+
				"	</transacao>"+
				"	<!--Optional:-->"+
				"	<dataAgendamentoEnvio>20/10/2014</dataAgendamentoEnvio>"+
				"	<!--Optional:-->"+
				"	<horaAgendamentoEnvio>22:30:00</horaAgendamentoEnvio>"+
				"	<!--You have a CHOICE of the next 2 items at this level-->"+
				"	<idCliente>"+
				"	   <idCliente>45</idCliente>"+
				"	</idCliente>"+
				"	<idFuncionario>"+
				"	   <idFuncionario>56</idFuncionario>"+
				"	</idFuncionario>"+
				"	<!--1 or more repetitions:-->"+
				"	<parametroModeloMensagem>"+
				"	   <idParametroModeloMensagem>Numero Conta</idParametroModeloMensagem>"+
				"	   <valorParametroModeloMensagem>5656566554343</valorParametroModeloMensagem>"+
				"	</parametroModeloMensagem>"+
				"  </notificacao>"+
				" </ws:solicitarNotificacao>";
		
		return requestStr;
	}

}
