package br.com.bancooriginal.notificacao.integration;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.core.PollableChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.bancooriginal.notificacao.integration.ftp.FTPOutboundGateway;

@ContextConfiguration({
	"classpath:META-INF/spring/integration-ftp-context.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class FtpBancoTest implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	
	@Test 
	public void testFtp() {
		
		FTPOutboundGateway ftpOutboundGateway = applicationContext.getBean(FTPOutboundGateway.class);
		
		List<Boolean> lista = ftpOutboundGateway.baixarArquivos("upload");
		
		System.out.println("Arquivos :" + lista.size()); 
	}
	
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	
}
