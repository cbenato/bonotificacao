package br.com.bancooriginal.notificacao.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.bancooriginal.notificacao.dto.SolicitacaoNotificacaoDTO;
import br.com.bancooriginal.notificacao.integration.ws.sms.SMSGateway;

@ContextConfiguration({
	"classpath:META-INF/spring/integration-context.xml",
	"classpath:META-INF/spring/integration-ws-context.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TWWSMSWebServiceTest implements ApplicationContextAware {

	private ApplicationContext applicationContext;
	
	@Autowired
	private SMSGateway smsGateway;
	
	@Test
	public void testTWWWebService() {
	
		SolicitacaoNotificacaoDTO solicitacaoNotificacao = new SolicitacaoNotificacaoDTO();
		
		String retorno = smsGateway.enviarSMS(solicitacaoNotificacao);  
		
		System.out.println(retorno);     
		
		/*MessageChannel channel = (MessageChannel)applicationContext.getBean("smsChannel");
		
		Message<String> message1 = MessageBuilder.withPayload("<solicitarNotificacao><notificacao><idSistema>CNTA</idSistema></notificacao></solicitarNotificacao>").build();	
		channel.send(message1);*/    
	}

	@Override   
	public void setApplicationContext(ApplicationContext arg0) 
			throws BeansException {
		applicationContext = arg0;    
		
	}

}