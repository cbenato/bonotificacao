package br.com.bancooriginal.notificacao.dto;

public class EventoDTO extends BaseNotificacaoDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8262665568804857765L;

	private Integer idEvento;

	public Integer getIdEvento() {
		return idEvento;
	}

	public void setIdEvento(Integer idEvento) {
		this.idEvento = idEvento;
	}
	
	
	
}
