package br.com.bancooriginal.notificacao.dto;

import java.util.Date;
import java.util.List;

public class SolicitacaoNotificacaoDTO extends BaseNotificacaoDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -938580698800489466L;
	private String idSistemaNotificador;
	private Integer codigoProduto;
	private String chaveProduto;
	private String email;
	private String telefone;
	private Date dataHoraAgendamentoEnvio;
	private EventoDTO evento;
	private TransacaoDTO transacao;
	private Integer idCliente;
	private Integer idFuncionario;
	private List<ParametroModeloMensagemDTO> parametrosModeloMensagem;
	public String getIdSistemaNotificador() {
		return idSistemaNotificador;
	}
	public void setIdSistemaNotificador(String idSistemaNotificador) {
		this.idSistemaNotificador = idSistemaNotificador;
	}
	public Integer getCodigoProduto() {
		return codigoProduto;
	}
	public void setCodigoProduto(Integer codigoProduto) {
		this.codigoProduto = codigoProduto;
	}
	public String getChaveProduto() {
		return chaveProduto;
	}
	public void setChaveProduto(String chaveProduto) {
		this.chaveProduto = chaveProduto;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public Date getDataHoraAgendamentoEnvio() {
		return dataHoraAgendamentoEnvio;
	}
	public void setDataHoraAgendamentoEnvio(Date dataHoraAgendamentoEnvio) {
		this.dataHoraAgendamentoEnvio = dataHoraAgendamentoEnvio;
	}
	public EventoDTO getEvento() {
		return evento;
	}
	public void setEvento(EventoDTO evento) {
		this.evento = evento;
	}
	public TransacaoDTO getTransacao() {
		return transacao;
	}
	public void setTransacao(TransacaoDTO transacao) {
		this.transacao = transacao;
	}
	public Integer getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}
	public Integer getIdFuncionario() {
		return idFuncionario;
	}
	public void setIdFuncionario(Integer idFuncionario) {
		this.idFuncionario = idFuncionario;
	}
	public List<ParametroModeloMensagemDTO> getParametrosModeloMensagem() {
		return parametrosModeloMensagem;
	}
	public void setParametrosModeloMensagem(
			List<ParametroModeloMensagemDTO> parametrosModeloMensagem) {
		this.parametrosModeloMensagem = parametrosModeloMensagem;
	}
	
	
}
