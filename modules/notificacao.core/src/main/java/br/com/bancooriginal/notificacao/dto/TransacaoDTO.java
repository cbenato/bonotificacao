package br.com.bancooriginal.notificacao.dto;

public class TransacaoDTO extends BaseNotificacaoDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8004482814403443538L;
	private Integer idBaseTransacao;
	private Integer idRazaoTransacao;
	private Double valorTransacao;
	public Integer getIdBaseTransacao() {
		return idBaseTransacao;
	}
	public void setIdBaseTransacao(Integer idBaseTransacao) {
		this.idBaseTransacao = idBaseTransacao;
	}
	public Integer getIdRazaoTransacao() {
		return idRazaoTransacao;
	}
	public void setIdRazaoTransacao(Integer idRazaoTransacao) {
		this.idRazaoTransacao = idRazaoTransacao;
	}
	public Double getValorTransacao() {
		return valorTransacao;
	}
	public void setValorTransacao(Double valorTransacao) {
		this.valorTransacao = valorTransacao;
	}
	
	
	
}
