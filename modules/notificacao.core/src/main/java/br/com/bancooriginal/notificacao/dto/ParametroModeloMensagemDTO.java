package br.com.bancooriginal.notificacao.dto;


public class ParametroModeloMensagemDTO extends BaseNotificacaoDTO {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4642925934751269873L;

	private String idParametroModeloMensagem;
	
	private String valorParametroModeloMensagem;

	public String getIdParametroModeloMensagem() {
		return idParametroModeloMensagem;
	}

	public void setIdParametroModeloMensagem(String idParametroModeloMensagem) {
		this.idParametroModeloMensagem = idParametroModeloMensagem;
	}

	public String getValorParametroModeloMensagem() {
		return valorParametroModeloMensagem;
	}

	public void setValorParametroModeloMensagem(String valorParametroModeloMensagem) {
		this.valorParametroModeloMensagem = valorParametroModeloMensagem;
	}
	
	
}
