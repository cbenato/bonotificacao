package br.com.bancooriginal.notificacao.batch;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import br.com.bancooriginal.notificacao.dto.SolicitacaoNotificacaoDTO;
import br.com.bancooriginal.notificacao.mensagem.modelo.GeradorMensagemService;

public class SolicitacaoNotificacaoEmLoteItemProcessor implements
		ItemProcessor<SolicitacaoNotificacaoDTO, SolicitacaoNotificacaoDTO> {

	private static final Logger logger = LoggerFactory.getLogger(SolicitacaoNotificacaoEmLoteItemProcessor.class);
	
	private GeradorMensagemService geradorMensagem;
	
	@Override
	public SolicitacaoNotificacaoDTO process(SolicitacaoNotificacaoDTO solicitacaoNotificacao)
			throws Exception {
		
		logger.debug("processando item...." + solicitacaoNotificacao);
		
		Map<String, String> parametros = new HashMap<String,String>();
		parametros.put("sistemaId", solicitacaoNotificacao.getIdSistemaNotificador().toString());
		
		
		//solicitacaoNotificacao.setMensagem(geradorMensagem.gerarMensagem("Teste de mensagem sistemaId: $sistemaId", parametros));
		
		return solicitacaoNotificacao;
	}

	public void setGeradorMensagem(GeradorMensagemService geradorMensagem) {
		this.geradorMensagem = geradorMensagem;
	}
	
	

}
