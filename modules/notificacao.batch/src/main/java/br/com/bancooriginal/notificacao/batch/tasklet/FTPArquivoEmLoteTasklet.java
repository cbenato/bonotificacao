package br.com.bancooriginal.notificacao.batch.tasklet;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import br.com.bancooriginal.notificacao.integration.ftp.FTPOutboundGateway;

public class FTPArquivoEmLoteTasklet implements Tasklet {

	
	private FTPOutboundGateway ftpGateway;
	private String diretorio;
	
	
	
	public void setDiretorio(String diretorio) {
		this.diretorio = diretorio;
	}



	public void setFtpGateway(FTPOutboundGateway ftpGateway) {
		this.ftpGateway = ftpGateway;
	}



	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1)
			throws Exception {
		
		ftpGateway.baixarArquivos(diretorio);
		
		return RepeatStatus.FINISHED;
	}

}
