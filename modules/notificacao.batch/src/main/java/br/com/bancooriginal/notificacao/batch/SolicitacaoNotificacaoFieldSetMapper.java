package br.com.bancooriginal.notificacao.batch;

import java.util.ArrayList;
import java.util.List;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import br.com.bancooriginal.notificacao.dto.EventoDTO;
import br.com.bancooriginal.notificacao.dto.ParametroModeloMensagemDTO;
import br.com.bancooriginal.notificacao.dto.SolicitacaoNotificacaoDTO;
import br.com.bancooriginal.notificacao.dto.TransacaoDTO;

public class SolicitacaoNotificacaoFieldSetMapper implements
		FieldSetMapper<SolicitacaoNotificacaoDTO> {

	@Override
	public SolicitacaoNotificacaoDTO mapFieldSet(FieldSet fieldSet) throws BindException {
		
		SolicitacaoNotificacaoDTO solicitacaoNotificacao = new SolicitacaoNotificacaoDTO();
		solicitacaoNotificacao.setChaveProduto(fieldSet.readString("chaveProduto"));
		solicitacaoNotificacao.setIdSistemaNotificador(fieldSet.readString("idSistema"));
		solicitacaoNotificacao.setIdCliente(fieldSet.readInt("idCliente"));
		
		
		EventoDTO evento = new EventoDTO();
		evento.setIdEvento(fieldSet.readInt("idEvento"));
		
		solicitacaoNotificacao.setEvento(evento);
		
		solicitacaoNotificacao.setIdFuncionario(fieldSet.readInt("idFuncionario"));
		
		TransacaoDTO transacao = new TransacaoDTO();
		transacao.setIdBaseTransacao(fieldSet.readInt("idBaseTransacao"));
		transacao.setIdBaseTransacao(fieldSet.readInt("idRazaoTransacao"));
		transacao.setValorTransacao(fieldSet.readDouble("valorTransacao"));
		
		solicitacaoNotificacao.setTransacao(transacao);
		solicitacaoNotificacao.setCodigoProduto(fieldSet.readInt("codigoProduto"));
		//solicitacaoNotificacao.setDataAgendamentoEnvio(dataAgendamentoEnvio);
		//solicitacaoNotificacao.setHoraAgendamentpEnvio(horaAgendamentpEnvio);
		solicitacaoNotificacao.setEmail(fieldSet.readString("email"));
		solicitacaoNotificacao.setTelefone(fieldSet.readString("telefone"));
		
		
		String [] names = fieldSet.getNames();
		int count = 0;
		
		List<ParametroModeloMensagemDTO> parametros = new ArrayList<ParametroModeloMensagemDTO>();
		
		for(String name: names){
			
			if(count > 10){
				String valor = fieldSet.readString(name);
				ParametroModeloMensagemDTO parametro = new ParametroModeloMensagemDTO();
				parametro.setIdParametroModeloMensagem(name);
				parametro.setValorParametroModeloMensagem(valor);
				parametros.add(parametro);
			}
			
			count++;
		}
		
		solicitacaoNotificacao.setParametrosModeloMensagem(parametros);
		
		return solicitacaoNotificacao;
	}

}
