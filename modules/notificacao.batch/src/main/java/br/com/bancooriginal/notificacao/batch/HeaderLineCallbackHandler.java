package br.com.bancooriginal.notificacao.batch;

import org.springframework.batch.item.file.LineCallbackHandler;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.beans.factory.annotation.Autowired;

public class HeaderLineCallbackHandler implements LineCallbackHandler {

	@Autowired
	public DelimitedLineTokenizer lineTokenizer;
	
	@Override
	public void handleLine(String line) {
		
		lineTokenizer.setNames(new String[]{});
				
		FieldSet fieldSet = lineTokenizer.tokenize(line);  
		
		String[] names = fieldSet.getValues();
		lineTokenizer.setNames(names);
		
		
	}

}
