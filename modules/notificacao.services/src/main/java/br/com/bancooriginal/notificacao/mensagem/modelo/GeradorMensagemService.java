package br.com.bancooriginal.notificacao.mensagem.modelo;

import java.util.Map;

public interface GeradorMensagemService {

	public String gerarMensagem(String modelo, Map<String,String> parametros) throws RuntimeException;
	
}
