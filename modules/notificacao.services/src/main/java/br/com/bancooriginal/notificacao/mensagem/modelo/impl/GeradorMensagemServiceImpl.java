package br.com.bancooriginal.notificacao.mensagem.modelo.impl;

import java.io.StringWriter;
import java.util.Map;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import br.com.bancooriginal.notificacao.mensagem.modelo.GeradorMensagemService;

public class GeradorMensagemServiceImpl implements GeradorMensagemService {

	public String gerarMensagem(
			String modelo, 
			Map<String, String> parametros)
			throws RuntimeException {
		
		String mensagem = null;
		
		try {

			VelocityContext velocityContext = new VelocityContext(parametros);
			StringWriter writer = new StringWriter();

		    Velocity.init();
		    Velocity.evaluate(velocityContext,
		                      writer,
		                      "LOG",  
		                      modelo);
		    mensagem = writer.getBuffer().toString();
		    
		} catch(Exception e){
			throw new RuntimeException(e);
		}
		
		
		return mensagem;
	}
	
}
