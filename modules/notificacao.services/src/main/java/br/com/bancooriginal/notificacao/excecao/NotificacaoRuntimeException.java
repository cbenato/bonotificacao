package br.com.bancooriginal.notificacao.excecao;

public class NotificacaoRuntimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7885561815981190748L;

	public NotificacaoRuntimeException() {
		super();
		
	}

	public NotificacaoRuntimeException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

	public NotificacaoRuntimeException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public NotificacaoRuntimeException(String message) {
		super(message);
		
	}

	public NotificacaoRuntimeException(Throwable cause) {
		super(cause);
		
	}

	
	
}
