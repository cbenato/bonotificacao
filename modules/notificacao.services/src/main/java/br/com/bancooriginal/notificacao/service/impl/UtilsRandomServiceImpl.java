package br.com.bancooriginal.notificacao.service.impl;

import br.com.bancooriginal.notificacao.service.UtilsRandomService;

/**
 * 
 * @author �lvaro
 *
 */
public class UtilsRandomServiceImpl implements UtilsRandomService {

	/**
	 * M�todo que retorna um n�mero aleat�rio entre 1 e o limite [1-limite]
	 * @param qtdCarac Quantos caracteres deve ter o retorono, ser� preenchido com zeros � esquerda
	 * @param limite At� que n�mero pode ser sorteado
	 * @return N�mero gerado em String, para manter os zeros � esquerda
	 */
	public final String getNumero(final int qtdCarac, final int limite) {
		try{
			String retorno = "";
			String concat;
			
			for (int i = 0; i < qtdCarac; i++)
				retorno += "0";
	
			concat = retorno.substring(0, retorno.length()-1);
			retorno = retorno + ((int) (Math.random() * (limite + 1)));
			retorno = retorno.substring(retorno.length() - qtdCarac, retorno.length());
			
			//Fa�o essa �ltima verifica��o para n�o retornar 0000, se retornar 0000, ele retorna 0001
			return (retorno == concat ? concat + "1" : retorno);
		}catch (Exception e) {
			return null;
		}
	}
}
