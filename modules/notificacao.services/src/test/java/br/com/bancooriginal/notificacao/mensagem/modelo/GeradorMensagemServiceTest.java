package br.com.bancooriginal.notificacao.mensagem.modelo;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/notificacao-services-context.xml"})
public class GeradorMensagemServiceTest implements ApplicationContextAware{

	private ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
	
	@Test
	public void testGerarMensagem() {
		
		 
		GeradorMensagemService gerador = (GeradorMensagemService)applicationContext.getBean("geradorMensagemService");
		
		Map<String, String> parametros = new HashMap<String,String>();
		parametros.put("mensagemNumero", "12345");
		parametros.put("evento", "12");
		
		String mensagem = gerador.gerarMensagem("Testando modelo da mensagem: $mensagemNumero do evento: $evento ", parametros);
		
		System.out.println(mensagem);
	}

}
