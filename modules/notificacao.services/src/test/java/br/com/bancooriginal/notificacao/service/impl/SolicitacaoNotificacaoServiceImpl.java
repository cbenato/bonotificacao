package br.com.bancooriginal.notificacao.service.impl;

import br.com.bancooriginal.notificacao.dao.SolicitacaoNotificacaoDAO;
import br.com.bancooriginal.notificacao.dto.SolicitacaoNotificacaoDTO;
import br.com.bancooriginal.notificacao.service.SolicitacaoNotificacaoService;

public class SolicitacaoNotificacaoServiceImpl implements
		SolicitacaoNotificacaoService {

	private SolicitacaoNotificacaoDAO solicitacaoNotificacaoDAO;
	
	
	
	public void setSolicitacaoNotificacaoDAO(
			SolicitacaoNotificacaoDAO solicitacaoNotificacaoDAO) {
		this.solicitacaoNotificacaoDAO = solicitacaoNotificacaoDAO;
	}



	@Override
	public Integer inserirSolicitacaoNotificacao(
			SolicitacaoNotificacaoDTO solicitacao) {
		
		return solicitacaoNotificacaoDAO.inserirSolicitacaoNotificacao(solicitacao);
	}

}
