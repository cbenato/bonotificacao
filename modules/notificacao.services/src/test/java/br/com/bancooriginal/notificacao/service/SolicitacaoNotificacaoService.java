package br.com.bancooriginal.notificacao.service;

import br.com.bancooriginal.notificacao.dto.SolicitacaoNotificacaoDTO;

public interface SolicitacaoNotificacaoService {
	
	public Integer inserirSolicitacaoNotificacao(SolicitacaoNotificacaoDTO solicitacao);

}
