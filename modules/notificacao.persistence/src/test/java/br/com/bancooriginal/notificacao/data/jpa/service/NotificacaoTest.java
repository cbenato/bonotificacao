package br.com.bancooriginal.notificacao.data.jpa.service;

import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Persistence;

import org.junit.Test;

import br.com.bancooriginal.notificacao.data.jpa.domain.Notificacao;

public class NotificacaoTest {

	@Test
	public void testIncluirNotificacao() {
		NotificacaoServiceImpl notServ = new NotificacaoServiceImpl();
		
		Map mapa = new HashMap<String, String>();
		
		mapa.put("javax.persistence.jdbc.driver","com.mysql.jdbc.Driver");
		mapa.put("javax.persistence.jdbc.url","jdbc:mysql://localhost/jpa");
		mapa.put("javax.persistence.jdbc.user","root");
		mapa.put("javax.persistence.jdbc.password","");
		
		mapa.put("hibernate.show_sql","true");
		mapa.put("hibernate.format_sql","true");
		mapa.put("hibernate.dialect","org.hibernate.dialect.MySQL5Dialect");
		mapa.put("hibernate.hbm2ddl.auto","validate");
		
		mapa.put("hibernate.c3p0.min_size","5");
		mapa.put("hibernate.c3p0.maz_size","10");
		mapa.put("hibernate.c3p0.timeout","300");
		mapa.put("hibernate.c3p0.max_statements","50");
		mapa.put("hibernate.c3p0.idle_test_period","2000"); 
			
				
		notServ.eManager = 
				Persistence.createEntityManagerFactory("notificacao",mapa).createEntityManager();
		
		Notificacao notificacao = new Notificacao();
//		notificacao.setChaveproduto("teste");
//		notificacao.setCodigoevento(0);
//		notificacao.setCodigogrupo(0);
//		notificacao.setCodigoproduto(0);
//		notificacao.setCodigostatus(0);
//		notificacao.setCodigotransacao(1);
//		notificacao.setDataalteracao(new Date());
//		notificacao.setDatainclusao(new Date());
//		notificacao.setDescricaonotificacao("teste");
//		notificacao.setNomenotificacao("teste");
//		notificacao.setNumeroordenacao((short)1);
		
		notServ.incluirNotificacao(notificacao);
	}

}
