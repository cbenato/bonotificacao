package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.ArquivoFornecedorMensagem;

public class ArquivoFornecedorMensagemDAO extends GenericDAO<ArquivoFornecedorMensagem>{

	public ArquivoFornecedorMensagemDAO() {
		super(ArquivoFornecedorMensagem.class);
	}
}
