package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the MENSAGEM database table.
 * 
 */
@Entity
@Table(name="MENSAGEM")
@NamedQuery(name="Mensagem.findAll", query="SELECT m FROM Mensagem m")
public class Mensagem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_MENSAGEM")
	private long codMensagem;

	@Column(name="DES_MENSAGEM")
	private String desMensagem;
	
	@Column(name="ID_MENSAGEM")
	private String idMensagem;
	
	

	//bi-directional many-to-one association to ModeloMensagem
	@ManyToOne
	@JoinColumn(name="ID_MODELO_MENSAGEM", referencedColumnName="ID_MODELO_MENSAGEM")
	private ModeloMensagem modeloMensagem;

	//bi-directional many-to-one association to MensagemNotificacao
	@OneToMany(mappedBy="mensagem")
	private List<MensagemNotificacao> mensagemNotificacaos;

	//bi-directional many-to-one association to ParametrizacaoMensagemNotificacao
	@OneToMany(mappedBy="mensagem")
	private List<ParametrizacaoMensagemNotificacao> parametrizacaoMensagemNotificacaos;

	public Mensagem() {
	}

	public long getCodMensagem() {
		return this.codMensagem;
	}

	public void setCodMensagem(long codMensagem) {
		this.codMensagem = codMensagem;
	}

	public String getDesMensagem() {
		return this.desMensagem;
	}

	public void setDesMensagem(String desMensagem) {
		this.desMensagem = desMensagem;
	}

	public ModeloMensagem getModeloMensagem() {
		return this.modeloMensagem;
	}

	public void setModeloMensagem(ModeloMensagem modeloMensagem) {
		this.modeloMensagem = modeloMensagem;
	}

	public List<MensagemNotificacao> getMensagemNotificacaos() {
		return this.mensagemNotificacaos;
	}

	public void setMensagemNotificacaos(List<MensagemNotificacao> mensagemNotificacaos) {
		this.mensagemNotificacaos = mensagemNotificacaos;
	}

	public MensagemNotificacao addMensagemNotificacao(MensagemNotificacao mensagemNotificacao) {
		getMensagemNotificacaos().add(mensagemNotificacao);
		mensagemNotificacao.setMensagem(this);

		return mensagemNotificacao;
	}

	public MensagemNotificacao removeMensagemNotificacao(MensagemNotificacao mensagemNotificacao) {
		getMensagemNotificacaos().remove(mensagemNotificacao);
		mensagemNotificacao.setMensagem(null);

		return mensagemNotificacao;
	}

	public List<ParametrizacaoMensagemNotificacao> getParametrizacaoMensagemNotificacaos() {
		return this.parametrizacaoMensagemNotificacaos;
	}

	public void setParametrizacaoMensagemNotificacaos(List<ParametrizacaoMensagemNotificacao> parametrizacaoMensagemNotificacaos) {
		this.parametrizacaoMensagemNotificacaos = parametrizacaoMensagemNotificacaos;
	}

	public ParametrizacaoMensagemNotificacao addParametrizacaoMensagemNotificacao(ParametrizacaoMensagemNotificacao parametrizacaoMensagemNotificacao) {
		getParametrizacaoMensagemNotificacaos().add(parametrizacaoMensagemNotificacao);
		parametrizacaoMensagemNotificacao.setMensagem(this);

		return parametrizacaoMensagemNotificacao;
	}

	public ParametrizacaoMensagemNotificacao removeParametrizacaoMensagemNotificacao(ParametrizacaoMensagemNotificacao parametrizacaoMensagemNotificacao) {
		getParametrizacaoMensagemNotificacaos().remove(parametrizacaoMensagemNotificacao);
		parametrizacaoMensagemNotificacao.setMensagem(null);

		return parametrizacaoMensagemNotificacao;
	}

}