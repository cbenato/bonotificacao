package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the Notificacao database table.
 * 
 */
@Entity
@NamedQuery(name="Notificacao.findAll", query="SELECT n FROM Notificacao n")
public class Notificacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_NOTIFICACAO")
	private long codNotificacao;

	@Column(name="COD_PRODUTO")
	private BigDecimal codProduto;

	@Column(name="COD_SITUACAO")
	private BigDecimal codSituacao;

	@Column(name="DAT_ALTERACAO")
	private Timestamp datAlteracao;

	@Column(name="DAT_INCLUSAO")
	private Timestamp datInclusao;

	@Column(name="DES_NOTIFICACAO")
	private String desNotificacao;

	@Column(name="IND_ONLINE")
	private String indOnline;

	@Column(name="NOM_NOTIFICACAO")
	private String nomNotificacao;

	@Column(name="NUM_ORDENACAO")
	private short numOrdenacao;

	//bi-directional many-to-one association to Evento
	@ManyToOne
	@JoinColumn(name="IDTFD_EVENTO", referencedColumnName="ID_EVENTO")
	private Evento evento;

	//bi-directional many-to-one association to GrupoTransacao
	@ManyToOne
	@JoinColumn(name="ID_GRUPO_TRANSACAO", referencedColumnName="ID_GRUPO_TRANSACAO")
	private GrupoTransacao grupoTransacao;

	//bi-directional many-to-one association to ParametrizacaoNotificacao
	@OneToMany(mappedBy="notificacao")
	private List<ParametrizacaoNotificacao> parametrizacaoNotificacaos;

	//bi-directional many-to-one association to SolicitacaoNotificacao
	@OneToMany(mappedBy="notificacao")
	private List<SolicitacaoNotificacao> solicitacaoNotificacaos;

	public Notificacao() {
	}

	public long getCodNotificacao() {
		return this.codNotificacao;
	}

	public void setCodNotificacao(long codNotificacao) {
		this.codNotificacao = codNotificacao;
	}

	public BigDecimal getCodProduto() {
		return this.codProduto;
	}

	public void setCodProduto(BigDecimal codProduto) {
		this.codProduto = codProduto;
	}

	public BigDecimal getCodSituacao() {
		return this.codSituacao;
	}

	public void setCodSituacao(BigDecimal codSituacao) {
		this.codSituacao = codSituacao;
	}

	public Timestamp getDatAlteracao() {
		return this.datAlteracao;
	}

	public void setDatAlteracao(Timestamp datAlteracao) {
		this.datAlteracao = datAlteracao;
	}

	public Timestamp getDatInclusao() {
		return this.datInclusao;
	}

	public void setDatInclusao(Timestamp datInclusao) {
		this.datInclusao = datInclusao;
	}

	public String getDesNotificacao() {
		return this.desNotificacao;
	}

	public void setDesNotificacao(String desNotificacao) {
		this.desNotificacao = desNotificacao;
	}

	public String getIndOnline() {
		return this.indOnline;
	}

	public void setIndOnline(String indOnline) {
		this.indOnline = indOnline;
	}

	public String getNomNotificacao() {
		return this.nomNotificacao;
	}

	public void setNomNotificacao(String nomNotificacao) {
		this.nomNotificacao = nomNotificacao;
	}

	public short getNumOrdenacao() {
		return this.numOrdenacao;
	}

	public void setNumOrdenacao(short numOrdenacao) {
		this.numOrdenacao = numOrdenacao;
	}

	public Evento getEvento() {
		return this.evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public GrupoTransacao getGrupoTransacao() {
		return this.grupoTransacao;
	}

	public void setGrupoTransacao(GrupoTransacao grupoTransacao) {
		this.grupoTransacao = grupoTransacao;
	}

	public List<ParametrizacaoNotificacao> getParametrizacaoNotificacaos() {
		return this.parametrizacaoNotificacaos;
	}

	public void setParametrizacaoNotificacaos(List<ParametrizacaoNotificacao> parametrizacaoNotificacaos) {
		this.parametrizacaoNotificacaos = parametrizacaoNotificacaos;
	}

	public ParametrizacaoNotificacao addParametrizacaoNotificacao(ParametrizacaoNotificacao parametrizacaoNotificacao) {
		getParametrizacaoNotificacaos().add(parametrizacaoNotificacao);
		parametrizacaoNotificacao.setNotificacao(this);

		return parametrizacaoNotificacao;
	}

	public ParametrizacaoNotificacao removeParametrizacaoNotificacao(ParametrizacaoNotificacao parametrizacaoNotificacao) {
		getParametrizacaoNotificacaos().remove(parametrizacaoNotificacao);
		parametrizacaoNotificacao.setNotificacao(null);

		return parametrizacaoNotificacao;
	}

	public List<SolicitacaoNotificacao> getSolicitacaoNotificacaos() {
		return this.solicitacaoNotificacaos;
	}

	public void setSolicitacaoNotificacaos(List<SolicitacaoNotificacao> solicitacaoNotificacaos) {
		this.solicitacaoNotificacaos = solicitacaoNotificacaos;
	}

	public SolicitacaoNotificacao addSolicitacaoNotificacao(SolicitacaoNotificacao solicitacaoNotificacao) {
		getSolicitacaoNotificacaos().add(solicitacaoNotificacao);
		solicitacaoNotificacao.setNotificacao(this);

		return solicitacaoNotificacao;
	}

	public SolicitacaoNotificacao removeSolicitacaoNotificacao(SolicitacaoNotificacao solicitacaoNotificacao) {
		getSolicitacaoNotificacaos().remove(solicitacaoNotificacao);
		solicitacaoNotificacao.setNotificacao(null);

		return solicitacaoNotificacao;
	}

}