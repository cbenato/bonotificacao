package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the FORNECEDOR_MENSAGEM database table.
 * 
 */
@Entity
@Table(name="FORNECEDOR_MENSAGEM")
@NamedQuery(name="FornecedorMensagem.findAll", query="SELECT f FROM FornecedorMensagem f")
public class FornecedorMensagem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_FORNECEDOR")
	private long codFornecedor;

	@Column(name="IND_ARQUIVO_LOTE")
	private String indArquivoLote;

	@Column(name="LOGIN_USUARIO")
	private String loginUsuario;

	@Column(name="NOM_FORNECEDOR")
	private String nomFornecedor;

	@Column(name="SENHA_USUARIO")
	private String senhaUsuario;

	//bi-directional many-to-one association to SituacaoFornecedorMensagem
	@OneToMany(mappedBy="fornecedorMensagem")
	private List<SituacaoFornecedorMensagem> situacaoFornecedorMensagems;

	//bi-directional many-to-one association to ArquivoFornecedorMensagem
	@OneToMany(mappedBy="fornecedorMensagem")
	private List<ArquivoFornecedorMensagem> arquivoFornecedorMensagems;

	//bi-directional many-to-one association to FornecedorFormaEnvioNotificacao
	@OneToMany(mappedBy="fornecedorMensagem")
	private List<FornecedorFormaEnvioNotificacao> fornecedorFormaEnvioNotificacaos;

	public FornecedorMensagem() {
	}

	public long getCodFornecedor() {
		return this.codFornecedor;
	}

	public void setCodFornecedor(long codFornecedor) {
		this.codFornecedor = codFornecedor;
	}

	public String getIndArquivoLote() {
		return this.indArquivoLote;
	}

	public void setIndArquivoLote(String indArquivoLote) {
		this.indArquivoLote = indArquivoLote;
	}

	public String getLoginUsuario() {
		return this.loginUsuario;
	}

	public void setLoginUsuario(String loginUsuario) {
		this.loginUsuario = loginUsuario;
	}

	public String getNomFornecedor() {
		return this.nomFornecedor;
	}

	public void setNomFornecedor(String nomFornecedor) {
		this.nomFornecedor = nomFornecedor;
	}

	public String getSenhaUsuario() {
		return this.senhaUsuario;
	}

	public void setSenhaUsuario(String senhaUsuario) {
		this.senhaUsuario = senhaUsuario;
	}

	public List<SituacaoFornecedorMensagem> getSituacaoFornecedorMensagems() {
		return this.situacaoFornecedorMensagems;
	}

	public void setSituacaoFornecedorMensagems(List<SituacaoFornecedorMensagem> situacaoFornecedorMensagems) {
		this.situacaoFornecedorMensagems = situacaoFornecedorMensagems;
	}

	public SituacaoFornecedorMensagem addSituacaoFornecedorMensagem(SituacaoFornecedorMensagem situacaoFornecedorMensagem) {
		getSituacaoFornecedorMensagems().add(situacaoFornecedorMensagem);
		situacaoFornecedorMensagem.setFornecedorMensagem(this);

		return situacaoFornecedorMensagem;
	}

	public SituacaoFornecedorMensagem removeSituacaoFornecedorMensagem(SituacaoFornecedorMensagem situacaoFornecedorMensagem) {
		getSituacaoFornecedorMensagems().remove(situacaoFornecedorMensagem);
		situacaoFornecedorMensagem.setFornecedorMensagem(null);

		return situacaoFornecedorMensagem;
	}

	public List<ArquivoFornecedorMensagem> getArquivoFornecedorMensagems() {
		return this.arquivoFornecedorMensagems;
	}

	public void setArquivoFornecedorMensagems(List<ArquivoFornecedorMensagem> arquivoFornecedorMensagems) {
		this.arquivoFornecedorMensagems = arquivoFornecedorMensagems;
	}

	public ArquivoFornecedorMensagem addArquivoFornecedorMensagem(ArquivoFornecedorMensagem arquivoFornecedorMensagem) {
		getArquivoFornecedorMensagems().add(arquivoFornecedorMensagem);
		arquivoFornecedorMensagem.setFornecedorMensagem(this);

		return arquivoFornecedorMensagem;
	}

	public ArquivoFornecedorMensagem removeArquivoFornecedorMensagem(ArquivoFornecedorMensagem arquivoFornecedorMensagem) {
		getArquivoFornecedorMensagems().remove(arquivoFornecedorMensagem);
		arquivoFornecedorMensagem.setFornecedorMensagem(null);

		return arquivoFornecedorMensagem;
	}

	public List<FornecedorFormaEnvioNotificacao> getFornecedorFormaEnvioNotificacaos() {
		return this.fornecedorFormaEnvioNotificacaos;
	}

	public void setFornecedorFormaEnvioNotificacaos(List<FornecedorFormaEnvioNotificacao> fornecedorFormaEnvioNotificacaos) {
		this.fornecedorFormaEnvioNotificacaos = fornecedorFormaEnvioNotificacaos;
	}

	public FornecedorFormaEnvioNotificacao addFornecedorFormaEnvioNotificacao(FornecedorFormaEnvioNotificacao fornecedorFormaEnvioNotificacao) {
		getFornecedorFormaEnvioNotificacaos().add(fornecedorFormaEnvioNotificacao);
		fornecedorFormaEnvioNotificacao.setFornecedorMensagem(this);

		return fornecedorFormaEnvioNotificacao;
	}

	public FornecedorFormaEnvioNotificacao removeFornecedorFormaEnvioNotificacao(FornecedorFormaEnvioNotificacao fornecedorFormaEnvioNotificacao) {
		getFornecedorFormaEnvioNotificacaos().remove(fornecedorFormaEnvioNotificacao);
		fornecedorFormaEnvioNotificacao.setFornecedorMensagem(null);

		return fornecedorFormaEnvioNotificacao;
	}

}