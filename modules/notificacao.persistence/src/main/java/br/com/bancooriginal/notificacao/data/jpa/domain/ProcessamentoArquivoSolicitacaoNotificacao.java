package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;


/**
 * The persistent class for the PROCESSAMENTO_ARQUIVO_SOLICITACAO_NOTIFICACAO database table.
 * 
 */
@Entity
@Table(name="PROCESSAMENTO_ARQUIVO_SOLICITACAO_NOTIFICACAO")
@NamedQuery(name="ProcessamentoArquivoSolicitacaoNotificacao.findAll", query="SELECT p FROM ProcessamentoArquivoSolicitacaoNotificacao p")
public class ProcessamentoArquivoSolicitacaoNotificacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ProcessamentoArquivoSolicitacaoNotificacaoPK id;

	@Column(name="COD_SITUACAO")
	private BigDecimal codSituacao;

	@Column(name="DAT_FIM_PROCESSAMENTO")
	private Timestamp datFimProcessamento;

	@Column(name="DAT_INICIO_PROCESSAMENTO")
	private Timestamp datInicioProcessamento;

	//bi-directional many-to-one association to ArquivoSolicitacaoNotificacao
	@ManyToOne
	@JoinColumn(name="COD_ARQUIVO_SOLICITACAO", insertable=false,updatable=false)
	private ArquivoSolicitacaoNotificacao arquivoSolicitacaoNotificacao;

	public ProcessamentoArquivoSolicitacaoNotificacao() {
	}

	public ProcessamentoArquivoSolicitacaoNotificacaoPK getId() {
		return this.id;
	}

	public void setId(ProcessamentoArquivoSolicitacaoNotificacaoPK id) {
		this.id = id;
	}

	public BigDecimal getCodSituacao() {
		return this.codSituacao;
	}

	public void setCodSituacao(BigDecimal codSituacao) {
		this.codSituacao = codSituacao;
	}

	public Timestamp getDatFimProcessamento() {
		return this.datFimProcessamento;
	}

	public void setDatFimProcessamento(Timestamp datFimProcessamento) {
		this.datFimProcessamento = datFimProcessamento;
	}

	public Timestamp getDatInicioProcessamento() {
		return this.datInicioProcessamento;
	}

	public void setDatInicioProcessamento(Timestamp datInicioProcessamento) {
		this.datInicioProcessamento = datInicioProcessamento;
	}

	public ArquivoSolicitacaoNotificacao getArquivoSolicitacaoNotificacao() {
		return this.arquivoSolicitacaoNotificacao;
	}

	public void setArquivoSolicitacaoNotificacao(ArquivoSolicitacaoNotificacao arquivoSolicitacaoNotificacao) {
		this.arquivoSolicitacaoNotificacao = arquivoSolicitacaoNotificacao;
	}

}