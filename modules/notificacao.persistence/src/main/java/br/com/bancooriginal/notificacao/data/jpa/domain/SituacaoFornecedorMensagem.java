package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the SITUACAO_FORNECEDOR_MENSAGEM database table.
 * 
 */
@Entity
@Table(name="SITUACAO_FORNECEDOR_MENSAGEM")
@NamedQuery(name="SituacaoFornecedorMensagem.findAll", query="SELECT s FROM SituacaoFornecedorMensagem s")
public class SituacaoFornecedorMensagem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_SITUACAO_FORNECEDOR")
	private long codSituacaoFornecedor;
	
	@Column(name="COD_FORNECEDOR")
	private long codFornecedor;
	
	@Column(name="DES_SITUACAO_FORNECEDOR")
	private String desSituacaoFornecedor;

	@Column(name="ID_FORMA_ENVIO_NOTIFICACAO")
	private BigDecimal idFormaEnvioNotificacao;

	@Column(name="NOM_SITUACAO_FORNECEDOR")
	private String nomSituacaoFornecedor;
	
	@Column(name="ID_SITUACAO_FORNECEDOR")
	private String idSituacaoFornecedor;
	

	//bi-directional many-to-one association to FormaEnvioNotificacao
	@ManyToOne
	@JoinColumn(name="ID_FORMA_ENVIO", referencedColumnName="ID_FORMA_ENVIO_NOTIFICACAO")
	private FormaEnvioNotificacao formaEnvioNotificacao;

	//bi-directional many-to-one association to FornecedorMensagem
	@ManyToOne
	@JoinColumn(name="COD_FORNECEDOR", insertable=false, updatable=false)
	private FornecedorMensagem fornecedorMensagem;

	//bi-directional many-to-one association to EnvioArquivoFornecedorMensagem
	@OneToMany(mappedBy="situacaoFornecedorMensagem")
	private List<EnvioArquivoFornecedorMensagem> envioArquivoFornecedorMensagems;

	//bi-directional many-to-one association to SolicitacaoMensagemNotificacao
	@OneToMany(mappedBy="situacaoFornecedorMensagem")
	private List<SolicitacaoMensagemNotificacao> solicitacaoMensagemNotificacaos;

	public SituacaoFornecedorMensagem() {
	}

	
	public String getIdSituacaoFornecedor() {
		return idSituacaoFornecedor;
	}


	public void setIdSituacaoFornecedor(String idSituacaoFornecedor) {
		this.idSituacaoFornecedor = idSituacaoFornecedor;
	}


	public long getCodSituacaoFornecedor() {
		return this.codSituacaoFornecedor;
	}

	public void setCodSituacaoFornecedor(long codSituacaoFornecedor) {
		this.codSituacaoFornecedor = codSituacaoFornecedor;
	}

	public String getDesSituacaoFornecedor() {
		return this.desSituacaoFornecedor;
	}

	public void setDesSituacaoFornecedor(String desSituacaoFornecedor) {
		this.desSituacaoFornecedor = desSituacaoFornecedor;
	}

	public BigDecimal getIdFormaEnvioNotificacao() {
		return this.idFormaEnvioNotificacao;
	}

	public void setIdFormaEnvioNotificacao(BigDecimal idFormaEnvioNotificacao) {
		this.idFormaEnvioNotificacao = idFormaEnvioNotificacao;
	}

	public String getNomSituacaoFornecedor() {
		return this.nomSituacaoFornecedor;
	}

	public void setNomSituacaoFornecedor(String nomSituacaoFornecedor) {
		this.nomSituacaoFornecedor = nomSituacaoFornecedor;
	}

	public FormaEnvioNotificacao getFormaEnvioNotificacao() {
		return this.formaEnvioNotificacao;
	}

	public void setFormaEnvioNotificacao(FormaEnvioNotificacao formaEnvioNotificacao) {
		this.formaEnvioNotificacao = formaEnvioNotificacao;
	}

	public FornecedorMensagem getFornecedorMensagem() {
		return this.fornecedorMensagem;
	}

	public void setFornecedorMensagem(FornecedorMensagem fornecedorMensagem) {
		this.fornecedorMensagem = fornecedorMensagem;
	}

	public List<EnvioArquivoFornecedorMensagem> getEnvioArquivoFornecedorMensagems() {
		return this.envioArquivoFornecedorMensagems;
	}

	public void setEnvioArquivoFornecedorMensagems(List<EnvioArquivoFornecedorMensagem> envioArquivoFornecedorMensagems) {
		this.envioArquivoFornecedorMensagems = envioArquivoFornecedorMensagems;
	}

	public EnvioArquivoFornecedorMensagem addEnvioArquivoFornecedorMensagem(EnvioArquivoFornecedorMensagem envioArquivoFornecedorMensagem) {
		getEnvioArquivoFornecedorMensagems().add(envioArquivoFornecedorMensagem);
		envioArquivoFornecedorMensagem.setSituacaoFornecedorMensagem(this);

		return envioArquivoFornecedorMensagem;
	}

	public EnvioArquivoFornecedorMensagem removeEnvioArquivoFornecedorMensagem(EnvioArquivoFornecedorMensagem envioArquivoFornecedorMensagem) {
		getEnvioArquivoFornecedorMensagems().remove(envioArquivoFornecedorMensagem);
		envioArquivoFornecedorMensagem.setSituacaoFornecedorMensagem(null);

		return envioArquivoFornecedorMensagem;
	}

	public List<SolicitacaoMensagemNotificacao> getSolicitacaoMensagemNotificacaos() {
		return this.solicitacaoMensagemNotificacaos;
	}

	public void setSolicitacaoMensagemNotificacaos(List<SolicitacaoMensagemNotificacao> solicitacaoMensagemNotificacaos) {
		this.solicitacaoMensagemNotificacaos = solicitacaoMensagemNotificacaos;
	}

	public SolicitacaoMensagemNotificacao addSolicitacaoMensagemNotificacao(SolicitacaoMensagemNotificacao solicitacaoMensagemNotificacao) {
		getSolicitacaoMensagemNotificacaos().add(solicitacaoMensagemNotificacao);
		solicitacaoMensagemNotificacao.setSituacaoFornecedorMensagem(this);

		return solicitacaoMensagemNotificacao;
	}

	public SolicitacaoMensagemNotificacao removeSolicitacaoMensagemNotificacao(SolicitacaoMensagemNotificacao solicitacaoMensagemNotificacao) {
		getSolicitacaoMensagemNotificacaos().remove(solicitacaoMensagemNotificacao);
		solicitacaoMensagemNotificacao.setSituacaoFornecedorMensagem(null);

		return solicitacaoMensagemNotificacao;
	}

}