package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.MensagemNotificacaoCurta;

public class MensagemNotificacaoCurtaDAO extends GenericDAO<MensagemNotificacaoCurta> {

	public MensagemNotificacaoCurtaDAO() {
		super(MensagemNotificacaoCurta.class);
	}

}