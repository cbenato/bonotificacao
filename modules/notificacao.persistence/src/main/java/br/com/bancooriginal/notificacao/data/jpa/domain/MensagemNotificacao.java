package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the MENSAGEM_NOTIFICACAO database table.
 * 
 */
@Entity
@Table(name="MENSAGEM_NOTIFICACAO")
@NamedQuery(name="MensagemNotificacao.findAll", query="SELECT m FROM MensagemNotificacao m")
public class MensagemNotificacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_SOLICITACAO_NOTIFICACAO")
	private long codSolicitacaoNotificacao;

	@Column(name="COD_FORMA_ENVIO_NOTIFICACAO")
	private BigDecimal codFormaEnvioNotificacao;

	@Column(name="COD_FORNECEDOR")
	private BigDecimal codFornecedor;

	@Column(name="COD_SITUACAO_FORNECEDOR")
	private BigDecimal codSituacaoFornecedor;

	@Column(name="CONTD_MENSAGEM")
	private String contdMensagem;

	//bi-directional one-to-one association to RespostaMensagemNotificacao
	@OneToOne(mappedBy="mensagemNotificacao")
	private RespostaMensagemNotificacao respostaMensagemNotificacao;

	//bi-directional many-to-one association to Mensagem
	@ManyToOne
	@JoinColumn(name="COD_MENSAGEM")
	private Mensagem mensagem;

	//bi-directional one-to-one association to SolicitacaoNotificacao
	@OneToOne
	@JoinColumn(name="COD_SOLICITACAO_NOTIFICACAO")
	private SolicitacaoNotificacao solicitacaoNotificacao;

	//bi-directional many-to-one association to ParametroMensagemNotifiacao
	@OneToMany(mappedBy="mensagemNotificacao")
	private List<ParametroMensagemNotificacao> parametroMensagemNotifiacaos;

	public MensagemNotificacao() {
	}

	public long getCodSolicitacaoNotificacao() {
		return this.codSolicitacaoNotificacao;
	}

	public void setCodSolicitacaoNotificacao(long codSolicitacaoNotificacao) {
		this.codSolicitacaoNotificacao = codSolicitacaoNotificacao;
	}

	public BigDecimal getCodFormaEnvioNotificacao() {
		return this.codFormaEnvioNotificacao;
	}

	public void setCodFormaEnvioNotificacao(BigDecimal codFormaEnvioNotificacao) {
		this.codFormaEnvioNotificacao = codFormaEnvioNotificacao;
	}

	public BigDecimal getCodFornecedor() {
		return this.codFornecedor;
	}

	public void setCodFornecedor(BigDecimal codFornecedor) {
		this.codFornecedor = codFornecedor;
	}

	public BigDecimal getCodSituacaoFornecedor() {
		return this.codSituacaoFornecedor;
	}

	public void setCodSituacaoFornecedor(BigDecimal codSituacaoFornecedor) {
		this.codSituacaoFornecedor = codSituacaoFornecedor;
	}

	public String getContdMensagem() {
		return this.contdMensagem;
	}

	public void setContdMensagem(String contdMensagem) {
		this.contdMensagem = contdMensagem;
	}

	public RespostaMensagemNotificacao getRespostaMensagemNotificacao() {
		return this.respostaMensagemNotificacao;
	}

	public void setRespostaMensagemNotificacao(RespostaMensagemNotificacao respostaMensagemNotificacao) {
		this.respostaMensagemNotificacao = respostaMensagemNotificacao;
	}

	public Mensagem getMensagem() {
		return this.mensagem;
	}

	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}

	public SolicitacaoNotificacao getSolicitacaoNotificacao() {
		return this.solicitacaoNotificacao;
	}

	public void setSolicitacaoNotificacao(SolicitacaoNotificacao solicitacaoNotificacao) {
		this.solicitacaoNotificacao = solicitacaoNotificacao;
	}

	public List<ParametroMensagemNotificacao> getParametroMensagemNotifiacaos() {
		return this.parametroMensagemNotifiacaos;
	}

	public void setParametroMensagemNotifiacaos(List<ParametroMensagemNotificacao> parametroMensagemNotifiacaos) {
		this.parametroMensagemNotifiacaos = parametroMensagemNotifiacaos;
	}

	public ParametroMensagemNotificacao addParametroMensagemNotifiacao(ParametroMensagemNotificacao parametroMensagemNotifiacao) {
		getParametroMensagemNotifiacaos().add(parametroMensagemNotifiacao);
		parametroMensagemNotifiacao.setMensagemNotificacao(this);

		return parametroMensagemNotifiacao;
	}

	public ParametroMensagemNotificacao removeParametroMensagemNotifiacao(ParametroMensagemNotificacao parametroMensagemNotifiacao) {
		getParametroMensagemNotifiacaos().remove(parametroMensagemNotifiacao);
		parametroMensagemNotifiacao.setMensagemNotificacao(null);

		return parametroMensagemNotifiacao;
	}

}