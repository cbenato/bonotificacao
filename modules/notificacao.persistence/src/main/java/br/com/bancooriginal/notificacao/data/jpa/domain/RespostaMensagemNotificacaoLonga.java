package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the RESPOSTA_MENSAGEM_NOTIFICACAO_LONGA database table.
 * 
 */
@Entity
@Table(name="RESPOSTA_MENSAGEM_NOTIFICACAO_LONGA")
@NamedQuery(name="RespostaMensagemNotificacaoLonga.findAll", query="SELECT r FROM RespostaMensagemNotificacaoLonga r")
public class RespostaMensagemNotificacaoLonga implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_SOLICITACAO_NOTIFICACAO")
	private long codSolicitacaoNotificacao;

	@Column(name="CONTD_RESPOSTA")
	private String contdResposta;

	public RespostaMensagemNotificacaoLonga() {
	}

	public long getCodSolicitacaoNotificacao() {
		return this.codSolicitacaoNotificacao;
	}

	public void setCodSolicitacaoNotificacao(long codSolicitacaoNotificacao) {
		this.codSolicitacaoNotificacao = codSolicitacaoNotificacao;
	}

	public String getContdResposta() {
		return this.contdResposta;
	}

	public void setContdResposta(String contdResposta) {
		this.contdResposta = contdResposta;
	}

}