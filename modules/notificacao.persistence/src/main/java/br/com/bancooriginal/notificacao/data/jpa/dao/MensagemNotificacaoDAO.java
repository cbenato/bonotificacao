package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.MensagemNotificacao;

public class MensagemNotificacaoDAO extends GenericDAO<MensagemNotificacao> {

	public MensagemNotificacaoDAO() {
		super(MensagemNotificacao.class);
	}

}