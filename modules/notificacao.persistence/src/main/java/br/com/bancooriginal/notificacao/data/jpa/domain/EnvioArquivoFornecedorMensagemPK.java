package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the ENVIO_ARQUIVO_FORNECEDOR_MENSAGEM database table.
 * 
 */
@Embeddable
public class EnvioArquivoFornecedorMensagemPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="COD_ENVIO_ARQUIVO")
	private String codEnvioArquivo;

	@Column(name="COD_ARQUIVO_FORNECEDOR", insertable=false, updatable=false)
	private long codArquivoFornecedor;

	public EnvioArquivoFornecedorMensagemPK() {
	}
	public String getCodEnvioArquivo() {
		return this.codEnvioArquivo;
	}
	public void setCodEnvioArquivo(String codEnvioArquivo) {
		this.codEnvioArquivo = codEnvioArquivo;
	}
	public long getCodArquivoFornecedor() {
		return this.codArquivoFornecedor;
	}
	public void setCodArquivoFornecedor(long codArquivoFornecedor) {
		this.codArquivoFornecedor = codArquivoFornecedor;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof EnvioArquivoFornecedorMensagemPK)) {
			return false;
		}
		EnvioArquivoFornecedorMensagemPK castOther = (EnvioArquivoFornecedorMensagemPK)other;
		return 
			this.codEnvioArquivo.equals(castOther.codEnvioArquivo)
			&& (this.codArquivoFornecedor == castOther.codArquivoFornecedor);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.codEnvioArquivo.hashCode();
		hash = hash * prime + ((int) (this.codArquivoFornecedor ^ (this.codArquivoFornecedor >>> 32)));
		
		return hash;
	}
}