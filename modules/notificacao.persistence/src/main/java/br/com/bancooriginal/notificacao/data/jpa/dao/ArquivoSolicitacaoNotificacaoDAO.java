package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.ArquivoSolicitacaoNotificacao;

public class ArquivoSolicitacaoNotificacaoDAO extends GenericDAO<ArquivoSolicitacaoNotificacao> {

	public ArquivoSolicitacaoNotificacaoDAO() {
		super(ArquivoSolicitacaoNotificacao.class);
	}

}
