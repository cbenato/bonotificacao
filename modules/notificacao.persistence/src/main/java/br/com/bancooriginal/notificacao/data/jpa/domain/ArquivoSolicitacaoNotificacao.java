package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the ARQUIVO_SOLICITACAO_NOTIFICACAO database table.
 * 
 */
@Entity
@Table(name="ARQUIVO_SOLICITACAO_NOTIFICACAO")
@NamedQuery(name="ArquivoSolicitacaoNotificacao.findAll", query="SELECT a FROM ArquivoSolicitacaoNotificacao a")
public class ArquivoSolicitacaoNotificacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_ARQUIVO_SOLICITACAO")
	private long codArquivoSolicitacao;

	@Column(name="COD_SISTEMA_SOLICITADOR")
	private String codSistemaSolicitador;

	@Column(name="DAT_RECEBIMENTO_ARQUIVO")
	private Timestamp datRecebimentoArquivo;

	@Column(name="NOM_ARQUIVO_SOLICITACAO")
	private String nomArquivoSolicitacao;

	//bi-directional many-to-one association to ProcessamentoArquivoSolicitacaoNotificacao
	@OneToMany(mappedBy="arquivoSolicitacaoNotificacao")
	private List<ProcessamentoArquivoSolicitacaoNotificacao> processamentoArquivoSolicitacaoNotificacaos;

	//bi-directional many-to-one association to SolicitacaoNotificacao
	@OneToMany(mappedBy="arquivoSolicitacaoNotificacao")
	private List<SolicitacaoNotificacao> solicitacaoNotificacaos;

	public ArquivoSolicitacaoNotificacao() {
	}

	public long getCodArquivoSolicitacao() {
		return this.codArquivoSolicitacao;
	}

	public void setCodArquivoSolicitacao(long codArquivoSolicitacao) {
		this.codArquivoSolicitacao = codArquivoSolicitacao;
	}

	public String getCodSistemaSolicitador() {
		return this.codSistemaSolicitador;
	}

	public void setCodSistemaSolicitador(String codSistemaSolicitador) {
		this.codSistemaSolicitador = codSistemaSolicitador;
	}

	public Timestamp getDatRecebimentoArquivo() {
		return this.datRecebimentoArquivo;
	}

	public void setDatRecebimentoArquivo(Timestamp datRecebimentoArquivo) {
		this.datRecebimentoArquivo = datRecebimentoArquivo;
	}

	public String getNomArquivoSolicitacao() {
		return this.nomArquivoSolicitacao;
	}

	public void setNomArquivoSolicitacao(String nomArquivoSolicitacao) {
		this.nomArquivoSolicitacao = nomArquivoSolicitacao;
	}

	public List<ProcessamentoArquivoSolicitacaoNotificacao> getProcessamentoArquivoSolicitacaoNotificacaos() {
		return this.processamentoArquivoSolicitacaoNotificacaos;
	}

	public void setProcessamentoArquivoSolicitacaoNotificacaos(List<ProcessamentoArquivoSolicitacaoNotificacao> processamentoArquivoSolicitacaoNotificacaos) {
		this.processamentoArquivoSolicitacaoNotificacaos = processamentoArquivoSolicitacaoNotificacaos;
	}

	public ProcessamentoArquivoSolicitacaoNotificacao addProcessamentoArquivoSolicitacaoNotificacao(ProcessamentoArquivoSolicitacaoNotificacao processamentoArquivoSolicitacaoNotificacao) {
		getProcessamentoArquivoSolicitacaoNotificacaos().add(processamentoArquivoSolicitacaoNotificacao);
		processamentoArquivoSolicitacaoNotificacao.setArquivoSolicitacaoNotificacao(this);

		return processamentoArquivoSolicitacaoNotificacao;
	}

	public ProcessamentoArquivoSolicitacaoNotificacao removeProcessamentoArquivoSolicitacaoNotificacao(ProcessamentoArquivoSolicitacaoNotificacao processamentoArquivoSolicitacaoNotificacao) {
		getProcessamentoArquivoSolicitacaoNotificacaos().remove(processamentoArquivoSolicitacaoNotificacao);
		processamentoArquivoSolicitacaoNotificacao.setArquivoSolicitacaoNotificacao(null);

		return processamentoArquivoSolicitacaoNotificacao;
	}

	public List<SolicitacaoNotificacao> getSolicitacaoNotificacaos() {
		return this.solicitacaoNotificacaos;
	}

	public void setSolicitacaoNotificacaos(List<SolicitacaoNotificacao> solicitacaoNotificacaos) {
		this.solicitacaoNotificacaos = solicitacaoNotificacaos;
	}

	public SolicitacaoNotificacao addSolicitacaoNotificacao(SolicitacaoNotificacao solicitacaoNotificacao) {
		getSolicitacaoNotificacaos().add(solicitacaoNotificacao);
		solicitacaoNotificacao.setArquivoSolicitacaoNotificacao(this);

		return solicitacaoNotificacao;
	}

	public SolicitacaoNotificacao removeSolicitacaoNotificacao(SolicitacaoNotificacao solicitacaoNotificacao) {
		getSolicitacaoNotificacaos().remove(solicitacaoNotificacao);
		solicitacaoNotificacao.setArquivoSolicitacaoNotificacao(null);

		return solicitacaoNotificacao;
	}

}