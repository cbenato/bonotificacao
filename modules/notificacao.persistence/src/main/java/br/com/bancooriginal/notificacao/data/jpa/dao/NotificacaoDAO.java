package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.Notificacao;

public class NotificacaoDAO extends GenericDAO<Notificacao> {

	public NotificacaoDAO() {
		super(Notificacao.class);
	}

}