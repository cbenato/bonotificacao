package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.FornecedorMensagem;

public class FornecedorMensagemDAO extends GenericDAO<FornecedorMensagem> {

	public FornecedorMensagemDAO() {
		super(FornecedorMensagem.class);
	}

}