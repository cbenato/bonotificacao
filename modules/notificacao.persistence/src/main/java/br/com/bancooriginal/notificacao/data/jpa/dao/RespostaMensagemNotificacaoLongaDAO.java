package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.RespostaMensagemNotificacaoLonga;

public class RespostaMensagemNotificacaoLongaDAO extends GenericDAO<RespostaMensagemNotificacaoLonga> {

	public RespostaMensagemNotificacaoLongaDAO() {
		super(RespostaMensagemNotificacaoLonga.class);
	}

}
