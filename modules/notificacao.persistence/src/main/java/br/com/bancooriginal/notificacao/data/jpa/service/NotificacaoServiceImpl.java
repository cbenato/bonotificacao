package br.com.bancooriginal.notificacao.data.jpa.service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.bancooriginal.notificacao.data.jpa.domain.Notificacao;

public class NotificacaoServiceImpl {

	@PersistenceContext(unitName="notificacao")
	EntityManager eManager;
	
	public void incluirNotificacao(Notificacao notificacao){
		eManager.getTransaction().begin();
		eManager.persist(notificacao);
		eManager.flush();
		eManager.getTransaction().commit();
	}
	
}
