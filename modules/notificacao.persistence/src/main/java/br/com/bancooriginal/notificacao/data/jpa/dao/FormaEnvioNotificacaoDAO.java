package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.FormaEnvioNotificacao;


public class FormaEnvioNotificacaoDAO extends GenericDAO<FormaEnvioNotificacao> {

	public FormaEnvioNotificacaoDAO() {
		super(FormaEnvioNotificacao.class);
	}

}