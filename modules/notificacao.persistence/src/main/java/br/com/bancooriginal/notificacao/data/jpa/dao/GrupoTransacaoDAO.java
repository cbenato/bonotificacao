package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.GrupoTransacao;


public class GrupoTransacaoDAO extends GenericDAO<GrupoTransacao> {

	public GrupoTransacaoDAO() {
		super(GrupoTransacao.class);
	}

}