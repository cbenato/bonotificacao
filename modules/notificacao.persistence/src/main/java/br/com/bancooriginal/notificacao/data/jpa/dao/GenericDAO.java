package br.com.bancooriginal.notificacao.data.jpa.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public abstract class GenericDAO<T> {
	private EntityManager em;

	public EntityManager getEM() {
		return this.em;
	}

	private Class<T> clazz;

	public GenericDAO(Class<T> clazz) {
		this.clazz = clazz;
	}

	public void salvar(T t) {
		em.persist(t);
	}

	public void remove(T t) {
		em.remove(t);
	}

	public T update(T t) {
		return em.merge(t);
	}

	public T find(Integer id) {
		return em.find(this.clazz, id);
	}

	@SuppressWarnings("unchecked")
	public List<T> consultarTodos(T t) {
		Query query = (Query) this.em.createNamedQuery(t.getClass().getSimpleName()+".findAll");
		return query.getResultList();
	}
}
