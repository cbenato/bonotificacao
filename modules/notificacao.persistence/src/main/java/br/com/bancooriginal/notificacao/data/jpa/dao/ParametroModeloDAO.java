package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.ParametroModelo;

public class ParametroModeloDAO extends GenericDAO<ParametroModelo> {

	public ParametroModeloDAO() {
		super(ParametroModelo.class);
	}

}
