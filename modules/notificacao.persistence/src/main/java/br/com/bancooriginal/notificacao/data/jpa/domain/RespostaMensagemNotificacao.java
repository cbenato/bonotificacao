package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the RESPOSTA_MENSAGEM_NOTIFICACAO database table.
 * 
 */
@Entity
@Table(name="RESPOSTA_MENSAGEM_NOTIFICACAO")
@NamedQuery(name="RespostaMensagemNotificacao.findAll", query="SELECT r FROM RespostaMensagemNotificacao r")
public class RespostaMensagemNotificacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_SOLICITACAO_NOTIFICACAO")
	private long codSolicitacaoNotificacao;

	@Column(name="DAT_RECEBIMENTO")
	private Timestamp datRecebimento;

	@Column(name="TIPO_TAMANHO_MENSAGEM")
	private String tipoTamanhoMensagem;

	//bi-directional one-to-one association to MensagemNotificacao
	@OneToOne
	@JoinColumn(name="COD_SOLICITACAO_NOTIFICACAO")
	private MensagemNotificacao mensagemNotificacao;

	public RespostaMensagemNotificacao() {
	}

	public long getCodSolicitacaoNotificacao() {
		return this.codSolicitacaoNotificacao;
	}

	public void setCodSolicitacaoNotificacao(long codSolicitacaoNotificacao) {
		this.codSolicitacaoNotificacao = codSolicitacaoNotificacao;
	}

	public Timestamp getDatRecebimento() {
		return this.datRecebimento;
	}

	public void setDatRecebimento(Timestamp datRecebimento) {
		this.datRecebimento = datRecebimento;
	}

	public String getTipoTamanhoMensagem() {
		return this.tipoTamanhoMensagem;
	}

	public void setTipoTamanhoMensagem(String tipoTamanhoMensagem) {
		this.tipoTamanhoMensagem = tipoTamanhoMensagem;
	}

	public MensagemNotificacao getMensagemNotificacao() {
		return this.mensagemNotificacao;
	}

	public void setMensagemNotificacao(MensagemNotificacao mensagemNotificacao) {
		this.mensagemNotificacao = mensagemNotificacao;
	}

}