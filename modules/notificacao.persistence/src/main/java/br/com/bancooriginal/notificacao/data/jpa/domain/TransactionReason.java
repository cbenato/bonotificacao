package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the TRANSACTION_REASON database table.
 * 
 */
@Entity
@Table(name="TRANSACTION_REASON")
@NamedQuery(name="TransactionReason.findAll", query="SELECT t FROM TransactionReason t")
public class TransactionReason implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID")
	private long id;

	@Column(name="LONG_DESC")
	private String longDesc;

	@Column(name="SHORT_DESC")
	private String shortDesc;
	
	@Column(name="TRANSACTION_CORE")
	private long transactionCore;
	
	@Column(name="REASON")
	private long reason;

	//bi-directional many-to-one association to SolicitacaoNotificacao
	@OneToMany(mappedBy="transactionReason")
	private List<SolicitacaoNotificacao> solicitacaoNotificacaos;

	public TransactionReason() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLongDesc() {
		return this.longDesc;
	}

	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}

	public String getShortDesc() {
		return this.shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public List<SolicitacaoNotificacao> getSolicitacaoNotificacaos() {
		return this.solicitacaoNotificacaos;
	}

	public void setSolicitacaoNotificacaos(List<SolicitacaoNotificacao> solicitacaoNotificacaos) {
		this.solicitacaoNotificacaos = solicitacaoNotificacaos;
	}

	public SolicitacaoNotificacao addSolicitacaoNotificacao(SolicitacaoNotificacao solicitacaoNotificacao) {
		getSolicitacaoNotificacaos().add(solicitacaoNotificacao);
		solicitacaoNotificacao.setTransactionReason(this);

		return solicitacaoNotificacao;
	}

	public SolicitacaoNotificacao removeSolicitacaoNotificacao(SolicitacaoNotificacao solicitacaoNotificacao) {
		getSolicitacaoNotificacaos().remove(solicitacaoNotificacao);
		solicitacaoNotificacao.setTransactionReason(null);

		return solicitacaoNotificacao;
	}

}