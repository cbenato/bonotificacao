package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the PARAMETRO_SOLICITACAO database table.
 * 
 */
@Entity
@Table(name="PARAMETRO_SOLICITACAO")
@NamedQuery(name="ParametroSolicitacao.findAll", query="SELECT p FROM ParametroSolicitacao p")
public class ParametroSolicitacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PARAMETRO_SOLICITACAO")
	private String idParametroSolicitacao;

	@Column(name="DES_PARAMETRO_SOLICITACAO")
	private String desParametroSolicitacao;

	@Column(name="IND_MASCARA")
	private String indMascara;

	@Column(name="TAMANHO_MAXIMO")
	private short tamanhoMaximo;

	//bi-directional many-to-one association to ParametroMensagemNotifiacao
	@OneToMany(mappedBy="parametroSolicitacao")
	private List<ParametroMensagemNotificacao> parametroMensagemNotifiacaos;

	//bi-directional many-to-many association to ParametroModelo
	@ManyToMany
	@JoinTable(
		name="PARAMETRO_MODELO_SOLICITACAO"
		, joinColumns={
			@JoinColumn(name="ID_PARAMETRO_SOLICITACAO")
			}
		, inverseJoinColumns={
			@JoinColumn(name="ID_MODELO_MENSAGEM", referencedColumnName="ID_MODELO_MENSAGEM"),
			@JoinColumn(name="ID_PARAMETRO_MODELO", referencedColumnName="ID_PARAMETRO_MODELO")
			}
		)
	private List<ParametroModelo> parametroModelos;

	public ParametroSolicitacao() {
	}

	public String getIdParametroSolicitacao() {
		return this.idParametroSolicitacao;
	}

	public void setIdParametroSolicitacao(String idParametroSolicitacao) {
		this.idParametroSolicitacao = idParametroSolicitacao;
	}

	public String getDesParametroSolicitacao() {
		return this.desParametroSolicitacao;
	}

	public void setDesParametroSolicitacao(String desParametroSolicitacao) {
		this.desParametroSolicitacao = desParametroSolicitacao;
	}

	public String getIndMascara() {
		return this.indMascara;
	}

	public void setIndMascara(String indMascara) {
		this.indMascara = indMascara;
	}

	public short getTamanhoMaximo() {
		return this.tamanhoMaximo;
	}

	public void setTamanhoMaximo(short tamanhoMaximo) {
		this.tamanhoMaximo = tamanhoMaximo;
	}

	public List<ParametroMensagemNotificacao> getParametroMensagemNotifiacaos() {
		return this.parametroMensagemNotifiacaos;
	}

	public void setParametroMensagemNotifiacaos(List<ParametroMensagemNotificacao> parametroMensagemNotifiacaos) {
		this.parametroMensagemNotifiacaos = parametroMensagemNotifiacaos;
	}

	public ParametroMensagemNotificacao addParametroMensagemNotifiacao(ParametroMensagemNotificacao parametroMensagemNotifiacao) {
		getParametroMensagemNotifiacaos().add(parametroMensagemNotifiacao);
		parametroMensagemNotifiacao.setParametroSolicitacao(this);

		return parametroMensagemNotifiacao;
	}

	public ParametroMensagemNotificacao removeParametroMensagemNotifiacao(ParametroMensagemNotificacao parametroMensagemNotifiacao) {
		getParametroMensagemNotifiacaos().remove(parametroMensagemNotifiacao);
		parametroMensagemNotifiacao.setParametroSolicitacao(null);

		return parametroMensagemNotifiacao;
	}

	public List<ParametroModelo> getParametroModelos() {
		return this.parametroModelos;
	}

	public void setParametroModelos(List<ParametroModelo> parametroModelos) {
		this.parametroModelos = parametroModelos;
	}

}