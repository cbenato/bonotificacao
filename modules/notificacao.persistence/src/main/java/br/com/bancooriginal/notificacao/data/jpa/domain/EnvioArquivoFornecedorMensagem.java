package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the ENVIO_ARQUIVO_FORNECEDOR_MENSAGEM database table.
 * 
 */
@Entity
@Table(name="ENVIO_ARQUIVO_FORNECEDOR_MENSAGEM")
@NamedQuery(name="EnvioArquivoFornecedorMensagem.findAll", query="SELECT e FROM EnvioArquivoFornecedorMensagem e")
public class EnvioArquivoFornecedorMensagem implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private EnvioArquivoFornecedorMensagemPK id;

	@Column(name="COD_SITUACAO")
	private BigDecimal codSituacao;
	
	@Column(name="DAT_ENVIO_ARQUIVO")
	private String datEnvioArquivo;

	@Column(name="ID_FORMA_ENVIO_NOTIFICACAO")
	private BigDecimal idFormaEnvioNotificacao;

	//bi-directional many-to-one association to ArquivoFornecedorMensagem
	@ManyToOne
	@JoinColumn(name="COD_ARQUIVO_FORNECEDOR", insertable=false, updatable=false)
	private ArquivoFornecedorMensagem arquivoFornecedorMensagem;

	//bi-directional many-to-one association to FormaEnvioNotificacao
	@ManyToOne
	@JoinColumn(name="ID_FORMA_ENVIO", referencedColumnName="ID_FORMA_ENVIO_NOTIFICACAO")
	private FormaEnvioNotificacao formaEnvioNotificacao;

	//bi-directional many-to-one association to SituacaoFornecedorMensagem
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="COD_FORNECEDOR", referencedColumnName="COD_FORNECEDOR"),
		@JoinColumn(name="ID_SITUACAO_FORNECEDOR", referencedColumnName="ID_SITUACAO_FORNECEDOR")
		})
	private SituacaoFornecedorMensagem situacaoFornecedorMensagem;

	public EnvioArquivoFornecedorMensagem() {
	}

	public EnvioArquivoFornecedorMensagemPK getId() {
		return this.id;
	}

	public void setId(EnvioArquivoFornecedorMensagemPK id) {
		this.id = id;
	}

	public BigDecimal getCodSituacao() {
		return this.codSituacao;
	}

	public void setCodSituacao(BigDecimal codSituacao) {
		this.codSituacao = codSituacao;
	}

	public String getDatEnvioArquivo() {
		return this.datEnvioArquivo;
	}

	public void setDatEnvioArquivo(String datEnvioArquivo) {
		this.datEnvioArquivo = datEnvioArquivo;
	}

	public BigDecimal getIdFormaEnvioNotificacao() {
		return this.idFormaEnvioNotificacao;
	}

	public void setIdFormaEnvioNotificacao(BigDecimal idFormaEnvioNotificacao) {
		this.idFormaEnvioNotificacao = idFormaEnvioNotificacao;
	}

	public ArquivoFornecedorMensagem getArquivoFornecedorMensagem() {
		return this.arquivoFornecedorMensagem;
	}

	public void setArquivoFornecedorMensagem(ArquivoFornecedorMensagem arquivoFornecedorMensagem) {
		this.arquivoFornecedorMensagem = arquivoFornecedorMensagem;
	}

	public FormaEnvioNotificacao getFormaEnvioNotificacao() {
		return this.formaEnvioNotificacao;
	}

	public void setFormaEnvioNotificacao(FormaEnvioNotificacao formaEnvioNotificacao) {
		this.formaEnvioNotificacao = formaEnvioNotificacao;
	}

	public SituacaoFornecedorMensagem getSituacaoFornecedorMensagem() {
		return this.situacaoFornecedorMensagem;
	}

	public void setSituacaoFornecedorMensagem(SituacaoFornecedorMensagem situacaoFornecedorMensagem) {
		this.situacaoFornecedorMensagem = situacaoFornecedorMensagem;
	}

}