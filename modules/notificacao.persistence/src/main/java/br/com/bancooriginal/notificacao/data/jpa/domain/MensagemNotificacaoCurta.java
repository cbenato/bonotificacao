package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the MENSAGEM_NOTIFICACAO_CURTA database table.
 * 
 */
@Entity
@Table(name="MENSAGEM_NOTIFICACAO_CURTA")
@NamedQuery(name="MensagemNotificacaoCurta.findAll", query="SELECT m FROM MensagemNotificacaoCurta m")
public class MensagemNotificacaoCurta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_NOTIFICACAO")
	private long codNotificacao;

	@Column(name="CONTD_MENSAGEM")
	private String contdMensagem;

	public MensagemNotificacaoCurta() {
	}

	public long getCodNotificacao() {
		return this.codNotificacao;
	}

	public void setCodNotificacao(long codNotificacao) {
		this.codNotificacao = codNotificacao;
	}

	public String getContdMensagem() {
		return this.contdMensagem;
	}

	public void setContdMensagem(String contdMensagem) {
		this.contdMensagem = contdMensagem;
	}

}