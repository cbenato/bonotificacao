package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.ParametroMensagemNotificacao;

public class ParametroMensagemNotificacaoDAO extends GenericDAO<ParametroMensagemNotificacao> {

	public ParametroMensagemNotificacaoDAO() {
		super(ParametroMensagemNotificacao.class);
	}

}
