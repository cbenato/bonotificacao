package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.SolicitacaoNotificacao;

public class SolicitacaoNotificacaoDAO extends GenericDAO<SolicitacaoNotificacao> {

	public SolicitacaoNotificacaoDAO() {
		super(SolicitacaoNotificacao.class);
	}

}

	