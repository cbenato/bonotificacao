package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.ModeloMensagemFornecedor;

public class ModeloMensagemFornecedorDAO extends GenericDAO<ModeloMensagemFornecedor> {

	public ModeloMensagemFornecedorDAO() {
		super(ModeloMensagemFornecedor.class);
	}

}
