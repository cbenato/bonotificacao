package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the PARAMETRO_MENSAGEM_NOTIFIACAO database table.
 * 
 */
@Embeddable
public class ParametroMensagemNotificacaoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ID_PARAMETRO_MENSAGEM", insertable=false, updatable=false)
	private String idParametroMensagem;

	@Column(name="COD_SOLICITACAO_NOTIFICACAO", insertable=false, updatable=false)
	private long codSolicitacaoNotificacao;

	public ParametroMensagemNotificacaoPK() {
	}
	public String getIdParametroMensagem() {
		return this.idParametroMensagem;
	}
	public void setIdParametroMensagem(String idParametroMensagem) {
		this.idParametroMensagem = idParametroMensagem;
	}
	public long getCodSolicitacaoNotificacao() {
		return this.codSolicitacaoNotificacao;
	}
	public void setCodSolicitacaoNotificacao(long codSolicitacaoNotificacao) {
		this.codSolicitacaoNotificacao = codSolicitacaoNotificacao;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ParametroMensagemNotificacaoPK)) {
			return false;
		}
		ParametroMensagemNotificacaoPK castOther = (ParametroMensagemNotificacaoPK)other;
		return 
			this.idParametroMensagem.equals(castOther.idParametroMensagem)
			&& (this.codSolicitacaoNotificacao == castOther.codSolicitacaoNotificacao);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idParametroMensagem.hashCode();
		hash = hash * prime + ((int) (this.codSolicitacaoNotificacao ^ (this.codSolicitacaoNotificacao >>> 32)));
		
		return hash;
	}
}