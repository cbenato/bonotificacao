package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.MensagemNotificacaoLonga;

public class MensagemNotificacaoLongaDAO extends GenericDAO<MensagemNotificacaoLonga> {

	public MensagemNotificacaoLongaDAO() {
		super(MensagemNotificacaoLonga.class);
	}

}