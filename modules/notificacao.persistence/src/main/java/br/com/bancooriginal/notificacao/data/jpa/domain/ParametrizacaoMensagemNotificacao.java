package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PARAMETRIZACAO_MENSAGEM_NOTIFICACAO database table.
 * 
 */
@Entity
@Table(name="PARAMETRIZACAO_MENSAGEM_NOTIFICACAO")
@NamedQuery(name="ParametrizacaoMensagemNotificacao.findAll", query="SELECT p FROM ParametrizacaoMensagemNotificacao p")
public class ParametrizacaoMensagemNotificacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ParametrizacaoMensagemNotificacaoPK id;

	//bi-directional many-to-one association to FormaEnvioNotificacao
	@ManyToOne
	@JoinColumn(name="ID_FORMA_ENVIO_NOTIFICACAO", referencedColumnName="ID_FORMA_ENVIO_NOTIFICACAO", insertable=false, updatable=false)
	private FormaEnvioNotificacao formaEnvioNotificacao;

	//bi-directional many-to-one association to Mensagem
	@ManyToOne
	@JoinColumn(name="ID_MENSAGEM", referencedColumnName="ID_MENSAGEM")
	private Mensagem mensagem;

	//bi-directional many-to-one association to ParametrizacaoNotificacao
	@ManyToOne
	@JoinColumn(name="COD_PARAMETRIZACAO_NOTIFICACAO",insertable=false, updatable=false)
	private ParametrizacaoNotificacao parametrizacaoNotificacao;

	public ParametrizacaoMensagemNotificacao() {
	}

	public ParametrizacaoMensagemNotificacaoPK getId() {
		return this.id;
	}

	public void setId(ParametrizacaoMensagemNotificacaoPK id) {
		this.id = id;
	}

	public FormaEnvioNotificacao getFormaEnvioNotificacao() {
		return this.formaEnvioNotificacao;
	}

	public void setFormaEnvioNotificacao(FormaEnvioNotificacao formaEnvioNotificacao) {
		this.formaEnvioNotificacao = formaEnvioNotificacao;
	}

	public Mensagem getMensagem() {
		return this.mensagem;
	}

	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}

	public ParametrizacaoNotificacao getParametrizacaoNotificacao() {
		return this.parametrizacaoNotificacao;
	}

	public void setParametrizacaoNotificacao(ParametrizacaoNotificacao parametrizacaoNotificacao) {
		this.parametrizacaoNotificacao = parametrizacaoNotificacao;
	}

}