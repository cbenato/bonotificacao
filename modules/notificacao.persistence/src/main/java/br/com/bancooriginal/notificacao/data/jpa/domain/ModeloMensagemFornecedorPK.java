package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the MODELO_MENSAGEM_FORNECEDOR database table.
 * 
 */
@Embeddable
public class ModeloMensagemFornecedorPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="COD_FORNECEDOR")
	private long codFornecedor;

	@Column(name="ID_MODELO_MENSAGEM")
	private long idModeloMensagem;

	public ModeloMensagemFornecedorPK() {
	}
	public long getCodFornecedor() {
		return this.codFornecedor;
	}
	public void setCodFornecedor(long codFornecedor) {
		this.codFornecedor = codFornecedor;
	}
	public long getIdModeloMensagem() {
		return this.idModeloMensagem;
	}
	public void setIdModeloMensagem(long idModeloMensagem) {
		this.idModeloMensagem = idModeloMensagem;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ModeloMensagemFornecedorPK)) {
			return false;
		}
		ModeloMensagemFornecedorPK castOther = (ModeloMensagemFornecedorPK)other;
		return 
			(this.codFornecedor == castOther.codFornecedor)
			&& (this.idModeloMensagem == castOther.idModeloMensagem);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.codFornecedor ^ (this.codFornecedor >>> 32)));
		hash = hash * prime + ((int) (this.idModeloMensagem ^ (this.idModeloMensagem >>> 32)));
		
		return hash;
	}
}