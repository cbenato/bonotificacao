package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the TRANSACAO_EVENTO database table.
 * 
 */
@Embeddable
public class TransacaoEventoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ID_BASE_TRANSACAO")
	private long idBaseTransacao;

	@Column(name="ID_RAZAO_TRANSACAO")
	private long idRazaoTransacao;

	public TransacaoEventoPK() {
	}
	public long getIdBaseTransacao() {
		return this.idBaseTransacao;
	}
	public void setIdBaseTransacao(long idBaseTransacao) {
		this.idBaseTransacao = idBaseTransacao;
	}
	public long getIdRazaoTransacao() {
		return this.idRazaoTransacao;
	}
	public void setIdRazaoTransacao(long idRazaoTransacao) {
		this.idRazaoTransacao = idRazaoTransacao;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TransacaoEventoPK)) {
			return false;
		}
		TransacaoEventoPK castOther = (TransacaoEventoPK)other;
		return 
			(this.idBaseTransacao == castOther.idBaseTransacao)
			&& (this.idRazaoTransacao == castOther.idRazaoTransacao);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.idBaseTransacao ^ (this.idBaseTransacao >>> 32)));
		hash = hash * prime + ((int) (this.idRazaoTransacao ^ (this.idRazaoTransacao >>> 32)));
		
		return hash;
	}
}