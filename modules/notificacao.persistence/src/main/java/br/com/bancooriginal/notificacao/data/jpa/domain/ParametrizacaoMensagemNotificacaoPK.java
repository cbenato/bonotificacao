package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the PARAMETRIZACAO_MENSAGEM_NOTIFICACAO database table.
 * 
 */
@Embeddable
public class ParametrizacaoMensagemNotificacaoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="COD_PARAMETRIZACAO_NOTIFICACAO", insertable=false, updatable=false)
	private long codParametrizacaoNotificacao;

	@Column(name="ID_FORMA_ENVIO_NOTIFICACAO", insertable=false, updatable=false)
	private long idFormaEnvioNotificacao;

	public ParametrizacaoMensagemNotificacaoPK() {
	}
	public long getCodParametrizacaoNotificacao() {
		return this.codParametrizacaoNotificacao;
	}
	public void setCodParametrizacaoNotificacao(long codParametrizacaoNotificacao) {
		this.codParametrizacaoNotificacao = codParametrizacaoNotificacao;
	}
	public long getIdFormaEnvioNotificacao() {
		return this.idFormaEnvioNotificacao;
	}
	public void setIdFormaEnvioNotificacao(long idFormaEnvioNotificacao) {
		this.idFormaEnvioNotificacao = idFormaEnvioNotificacao;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ParametrizacaoMensagemNotificacaoPK)) {
			return false;
		}
		ParametrizacaoMensagemNotificacaoPK castOther = (ParametrizacaoMensagemNotificacaoPK)other;
		return 
			(this.codParametrizacaoNotificacao == castOther.codParametrizacaoNotificacao)
			&& (this.idFormaEnvioNotificacao == castOther.idFormaEnvioNotificacao);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.codParametrizacaoNotificacao ^ (this.codParametrizacaoNotificacao >>> 32)));
		hash = hash * prime + ((int) (this.idFormaEnvioNotificacao ^ (this.idFormaEnvioNotificacao >>> 32)));
		
		return hash;
	}
}