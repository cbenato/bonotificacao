package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the EVENTO database table.
 * 
 */
@Entity
@Table(name="EVENTO")
@NamedQuery(name="Evento.findAll", query="SELECT e FROM Evento e")
public class Evento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_EVENTO")
	private long codEvento;

	@Column(name="DES_EVENTO")
	private String desEvento;

	@Column(name="IND_SEGURANCA")
	private String indSeguranca;

	@Column(name="IND_TIPO_DESTINATARIO")
	private String indTipoDestinatario;

	@Column(name="NOM_EVENTO")
	private String nomEvento;
	
	@Column(name="ID_EVENTO")
	private long idEvento;

	//bi-directional many-to-one association to ConfiguracaoNotificacao
	@OneToMany(mappedBy="evento")
	private List<ConfiguracaoNotificacao> configuracaoNotificacaos;

	//bi-directional many-to-one association to Notificacao
	@OneToMany(mappedBy="evento")
	private List<Notificacao> notificacaos;

	//bi-directional many-to-one association to SolicitacaoNotificacao
	@OneToMany(mappedBy="evento")
	private List<SolicitacaoNotificacao> solicitacaoNotificacaos;

	public Evento() {
	}

	public long getCodEvento() {
		return this.codEvento;
	}

	public void setCodEvento(long codEvento) {
		this.codEvento = codEvento;
	}

	public String getDesEvento() {
		return this.desEvento;
	}

	public void setDesEvento(String desEvento) {
		this.desEvento = desEvento;
	}

	public String getIndSeguranca() {
		return this.indSeguranca;
	}

	public void setIndSeguranca(String indSeguranca) {
		this.indSeguranca = indSeguranca;
	}

	public String getIndTipoDestinatario() {
		return this.indTipoDestinatario;
	}

	public void setIndTipoDestinatario(String indTipoDestinatario) {
		this.indTipoDestinatario = indTipoDestinatario;
	}

	public String getNomEvento() {
		return this.nomEvento;
	}

	public void setNomEvento(String nomEvento) {
		this.nomEvento = nomEvento;
	}

	public List<ConfiguracaoNotificacao> getConfiguracaoNotificacaos() {
		return this.configuracaoNotificacaos;
	}

	public void setConfiguracaoNotificacaos(List<ConfiguracaoNotificacao> configuracaoNotificacaos) {
		this.configuracaoNotificacaos = configuracaoNotificacaos;
	}

	public ConfiguracaoNotificacao addConfiguracaoNotificacao(ConfiguracaoNotificacao configuracaoNotificacao) {
		getConfiguracaoNotificacaos().add(configuracaoNotificacao);
		configuracaoNotificacao.setEvento(this);

		return configuracaoNotificacao;
	}

	public ConfiguracaoNotificacao removeConfiguracaoNotificacao(ConfiguracaoNotificacao configuracaoNotificacao) {
		getConfiguracaoNotificacaos().remove(configuracaoNotificacao);
		configuracaoNotificacao.setEvento(null);

		return configuracaoNotificacao;
	}

	public List<Notificacao> getNotificacaos() {
		return this.notificacaos;
	}

	public void setNotificacaos(List<Notificacao> notificacaos) {
		this.notificacaos = notificacaos;
	}

	public Notificacao addNotificacao(Notificacao notificacao) {
		getNotificacaos().add(notificacao);
		notificacao.setEvento(this);

		return notificacao;
	}

	public Notificacao removeNotificacao(Notificacao notificacao) {
		getNotificacaos().remove(notificacao);
		notificacao.setEvento(null);

		return notificacao;
	}

	public List<SolicitacaoNotificacao> getSolicitacaoNotificacaos() {
		return this.solicitacaoNotificacaos;
	}

	public void setSolicitacaoNotificacaos(List<SolicitacaoNotificacao> solicitacaoNotificacaos) {
		this.solicitacaoNotificacaos = solicitacaoNotificacaos;
	}

	public SolicitacaoNotificacao addSolicitacaoNotificacao(SolicitacaoNotificacao solicitacaoNotificacao) {
		getSolicitacaoNotificacaos().add(solicitacaoNotificacao);
		solicitacaoNotificacao.setEvento(this);

		return solicitacaoNotificacao;
	}

	public SolicitacaoNotificacao removeSolicitacaoNotificacao(SolicitacaoNotificacao solicitacaoNotificacao) {
		getSolicitacaoNotificacaos().remove(solicitacaoNotificacao);
		solicitacaoNotificacao.setEvento(null);

		return solicitacaoNotificacao;
	}

}