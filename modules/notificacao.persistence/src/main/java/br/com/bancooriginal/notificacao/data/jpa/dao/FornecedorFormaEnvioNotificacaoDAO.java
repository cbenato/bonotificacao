package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.FornecedorFormaEnvioNotificacao;

public class FornecedorFormaEnvioNotificacaoDAO extends GenericDAO<FornecedorFormaEnvioNotificacao> {

	public FornecedorFormaEnvioNotificacaoDAO() {
		super(FornecedorFormaEnvioNotificacao.class);
	}

}