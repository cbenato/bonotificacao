package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the PARAMETRO_MODELO database table.
 * 
 */
@Embeddable
public class ParametroModeloPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ID_PARAMETRO_MODELO")
	private String idParametroModelo;

	@Column(name="ID_MODELO_MENSAGEM", insertable=false, updatable=false)
	private long idModeloMensagem;

	public ParametroModeloPK() {
	}
	public String getIdParametroModelo() {
		return this.idParametroModelo;
	}
	public void setIdParametroModelo(String idParametroModelo) {
		this.idParametroModelo = idParametroModelo;
	}
	public long getIdModeloMensagem() {
		return this.idModeloMensagem;
	}
	public void setIdModeloMensagem(long idModeloMensagem) {
		this.idModeloMensagem = idModeloMensagem;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ParametroModeloPK)) {
			return false;
		}
		ParametroModeloPK castOther = (ParametroModeloPK)other;
		return 
			this.idParametroModelo.equals(castOther.idParametroModelo)
			&& (this.idModeloMensagem == castOther.idModeloMensagem);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idParametroModelo.hashCode();
		hash = hash * prime + ((int) (this.idModeloMensagem ^ (this.idModeloMensagem >>> 32)));
		
		return hash;
	}
}