package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.TipoMensagem;

public class TipoMensagemDAO extends GenericDAO<TipoMensagem> {

	public TipoMensagemDAO() {
		super(TipoMensagem.class);
	}

}