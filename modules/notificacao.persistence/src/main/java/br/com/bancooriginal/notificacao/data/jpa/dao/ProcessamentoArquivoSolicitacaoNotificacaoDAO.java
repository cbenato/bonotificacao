package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.ProcessamentoArquivoSolicitacaoNotificacao;

public class ProcessamentoArquivoSolicitacaoNotificacaoDAO extends GenericDAO<ProcessamentoArquivoSolicitacaoNotificacao> {

	public ProcessamentoArquivoSolicitacaoNotificacaoDAO() {
		super(ProcessamentoArquivoSolicitacaoNotificacao.class);
	}

}
