package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the MODELO_MENSAGEM_INTERNO database table.
 * 
 */
@Entity
@Table(name="MODELO_MENSAGEM_INTERNO")
@NamedQuery(name="ModeloMensagemInterno.findAll", query="SELECT m FROM ModeloMensagemInterno m")
public class ModeloMensagemInterno implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_MODELO_MENSAGEM")
	private long idModeloMensagem;

	@Column(name="TIPO_TAMANHO_MENSAGEM")
	private String tipoTamanhoMensagem;

	public ModeloMensagemInterno() {
	}

	public long getIdModeloMensagem() {
		return this.idModeloMensagem;
	}

	public void setIdModeloMensagem(long idModeloMensagem) {
		this.idModeloMensagem = idModeloMensagem;
	}

	public String getTipoTamanhoMensagem() {
		return this.tipoTamanhoMensagem;
	}

	public void setTipoTamanhoMensagem(String tipoTamanhoMensagem) {
		this.tipoTamanhoMensagem = tipoTamanhoMensagem;
	}

}