package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.Mensagem;

public class MensagemDAO extends GenericDAO<Mensagem> {

	public MensagemDAO() {
		super(Mensagem.class);
	}

}