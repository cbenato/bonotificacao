package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.ParametrizacaoMensagemNotificacao;

public class ParametrizacaoMensagemNotificacaoDAO extends GenericDAO<ParametrizacaoMensagemNotificacao> {

	public ParametrizacaoMensagemNotificacaoDAO() {
		super(ParametrizacaoMensagemNotificacao.class);
	}

}
