package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the MODELO_MENSAGEM_FORNECEDOR database table.
 * 
 */
@Entity
@Table(name="MODELO_MENSAGEM_FORNECEDOR")
@NamedQuery(name="ModeloMensagemFornecedor.findAll", query="SELECT m FROM ModeloMensagemFornecedor m")
public class ModeloMensagemFornecedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ModeloMensagemFornecedorPK id;

	@Column(name="COD_MODELO_MENSAGEM_FORNECEDOR")
	private BigDecimal codModeloMensagemFornecedor;

	public ModeloMensagemFornecedor() {
	}

	public ModeloMensagemFornecedorPK getId() {
		return this.id;
	}

	public void setId(ModeloMensagemFornecedorPK id) {
		this.id = id;
	}

	public BigDecimal getCodModeloMensagemFornecedor() {
		return this.codModeloMensagemFornecedor;
	}

	public void setCodModeloMensagemFornecedor(BigDecimal codModeloMensagemFornecedor) {
		this.codModeloMensagemFornecedor = codModeloMensagemFornecedor;
	}

}