package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.EnvioArquivoFornecedorMensagem;

public class EnvioArquivoFornecedorMensagemDAO extends GenericDAO<EnvioArquivoFornecedorMensagem> {

	public EnvioArquivoFornecedorMensagemDAO() {
		super(EnvioArquivoFornecedorMensagem.class);
	}

}
