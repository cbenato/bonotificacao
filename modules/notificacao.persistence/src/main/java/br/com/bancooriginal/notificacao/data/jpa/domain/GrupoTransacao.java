package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the GRUPO_TRANSACAO database table.
 * 
 */
@Entity
@Table(name="GRUPO_TRANSACAO")
@NamedQuery(name="GrupoTransacao.findAll", query="SELECT g FROM GrupoTransacao g")
public class GrupoTransacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_GRUPO_TRANSACAO")
	private long codGrupoTransacao;

	@Column(name="DES_GRUPO")
	private String desGrupo;

	@Column(name="NOM_GRUPO")
	private String nomGrupo;

	@Column(name="ID_GRUPO_TRANSACAO")
	private long idGrupoTransacao;
	
	//bi-directional many-to-one association to GrupoTransacao
	@ManyToOne
	@JoinColumn(name="ID_GRUPO_SUPERIOR", referencedColumnName="ID_GRUPO_TRANSACAO")
	private GrupoTransacao grupoTransacao;

	//bi-directional many-to-one association to GrupoTransacao
	@OneToMany(mappedBy="grupoTransacao")
	private List<GrupoTransacao> grupoTransacaos;

	//bi-directional many-to-one association to Notificacao
	@OneToMany(mappedBy="grupoTransacao")
	private List<Notificacao> notificacaos;

	public GrupoTransacao() {
	}

	public long getCodGrupoTransacao() {
		return this.codGrupoTransacao;
	}

	public void setCodGrupoTransacao(long codGrupoTransacao) {
		this.codGrupoTransacao = codGrupoTransacao;
	}

	public String getDesGrupo() {
		return this.desGrupo;
	}

	public void setDesGrupo(String desGrupo) {
		this.desGrupo = desGrupo;
	}

	public String getNomGrupo() {
		return this.nomGrupo;
	}

	public void setNomGrupo(String nomGrupo) {
		this.nomGrupo = nomGrupo;
	}

	public GrupoTransacao getGrupoTransacao() {
		return this.grupoTransacao;
	}

	public void setGrupoTransacao(GrupoTransacao grupoTransacao) {
		this.grupoTransacao = grupoTransacao;
	}

	public List<GrupoTransacao> getGrupoTransacaos() {
		return this.grupoTransacaos;
	}

	public void setGrupoTransacaos(List<GrupoTransacao> grupoTransacaos) {
		this.grupoTransacaos = grupoTransacaos;
	}

	public GrupoTransacao addGrupoTransacao(GrupoTransacao grupoTransacao) {
		getGrupoTransacaos().add(grupoTransacao);
		grupoTransacao.setGrupoTransacao(this);

		return grupoTransacao;
	}

	public GrupoTransacao removeGrupoTransacao(GrupoTransacao grupoTransacao) {
		getGrupoTransacaos().remove(grupoTransacao);
		grupoTransacao.setGrupoTransacao(null);

		return grupoTransacao;
	}

	public List<Notificacao> getNotificacaos() {
		return this.notificacaos;
	}

	public void setNotificacaos(List<Notificacao> notificacaos) {
		this.notificacaos = notificacaos;
	}

	public Notificacao addNotificacao(Notificacao notificacao) {
		getNotificacaos().add(notificacao);
		notificacao.setGrupoTransacao(this);

		return notificacao;
	}

	public Notificacao removeNotificacao(Notificacao notificacao) {
		getNotificacaos().remove(notificacao);
		notificacao.setGrupoTransacao(null);

		return notificacao;
	}

}