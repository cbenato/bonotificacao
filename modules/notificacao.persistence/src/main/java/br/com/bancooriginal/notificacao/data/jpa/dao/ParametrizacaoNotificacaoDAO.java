package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.ParametrizacaoNotificacao;


public class ParametrizacaoNotificacaoDAO extends GenericDAO<ParametrizacaoNotificacao> {

	public ParametrizacaoNotificacaoDAO() {
		super(ParametrizacaoNotificacao.class);
	}

}
