package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the SOLICITACAO_NOTIFICACAO database table.
 * 
 */
@Entity
@Table(name="SOLICITACAO_NOTIFICACAO")
@NamedQuery(name="SolicitacaoNotificacao.findAll", query="SELECT s FROM SolicitacaoNotificacao s")
public class SolicitacaoNotificacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_SOLICITACAO_NOTIFICACAO")
	private long codSolicitacaoNotificacao;

	@Column(name="CHAVE_PRODUTO")
	private String chaveProduto;

	@Column(name="COD_PRODUTO")
	private BigDecimal codProduto;

	@Column(name="COD_SITUACAO")
	private BigDecimal codSituacao;

	@Column(name="DAT_AGENDAMENTO_ENVIO")
	private Timestamp datAgendamentoEnvio;

	@Column(name="DAT_SOLICITACAO")
	private Timestamp datSolicitacao;

	@Column(name="END_EMAIL")
	private String endEmail;

	@Column(name="ID_CLIENTE")
	private String idCliente;

	@Column(name="ID_FUNCIONARIO")
	private String idFuncionario;

	@Column(name="ID_RESPONSAVEL")
	private String idResponsavel;

	@Column(name="ID_SISTEMA_SOLICITADOR")
	private String idSistemaSolicitador;

	@Column(name="NUM_TELEFONE_MOVEL")
	private String numTelefoneMovel;

	@Column(name="VAL_TRANSACAO")
	private BigDecimal valTransacao;

	//bi-directional one-to-one association to MensagemNotificacao
	@OneToOne(mappedBy="solicitacaoNotificacao")
	private MensagemNotificacao mensagemNotificacao;

	//bi-directional many-to-one association to SolicitacaoMensagemNotificacao
	@OneToMany(mappedBy="solicitacaoNotificacao")
	private List<SolicitacaoMensagemNotificacao> solicitacaoMensagemNotificacaos;

	//bi-directional many-to-one association to ArquivoFornecedorMensagem
	@ManyToOne
	@JoinColumn(name="COD_ARQUIVO_FORNECEDOR")
	private ArquivoFornecedorMensagem arquivoFornecedorMensagem;

	//bi-directional many-to-one association to ArquivoSolicitacaoNotificacao
	@ManyToOne
	@JoinColumn(name="COD_ARQUIVO_SOLICITACAO")
	private ArquivoSolicitacaoNotificacao arquivoSolicitacaoNotificacao;

	//bi-directional many-to-one association to ConfiguracaoNotificacao
	@ManyToOne
	@JoinColumn(name="COD_CONFIGURACAO_NOTIFICACAO")
	private ConfiguracaoNotificacao configuracaoNotificacao;

	//bi-directional many-to-one association to Evento
	@ManyToOne
	@JoinColumn(name="ID_EVENTO", referencedColumnName="ID_EVENTO")
	private Evento evento;

	//bi-directional many-to-one association to Notificacao
	@ManyToOne
	@JoinColumn(name="COD_NOTIFICACAO")
	private Notificacao notificacao;

	//bi-directional many-to-one association to TransactionReason
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="ID_BASE_TRANSACAO", referencedColumnName="TRANSACTION_CORE"),
		@JoinColumn(name="ID_RAZAO_TRANSACAO", referencedColumnName="REASON")
		})
	private TransactionReason transactionReason;

	public SolicitacaoNotificacao() {
	}

	public long getCodSolicitacaoNotificacao() {
		return this.codSolicitacaoNotificacao;
	}

	public void setCodSolicitacaoNotificacao(long codSolicitacaoNotificacao) {
		this.codSolicitacaoNotificacao = codSolicitacaoNotificacao;
	}

	public String getChaveProduto() {
		return this.chaveProduto;
	}

	public void setChaveProduto(String chaveProduto) {
		this.chaveProduto = chaveProduto;
	}

	public BigDecimal getCodProduto() {
		return this.codProduto;
	}

	public void setCodProduto(BigDecimal codProduto) {
		this.codProduto = codProduto;
	}

	public BigDecimal getCodSituacao() {
		return this.codSituacao;
	}

	public void setCodSituacao(BigDecimal codSituacao) {
		this.codSituacao = codSituacao;
	}

	public Timestamp getDatAgendamentoEnvio() {
		return this.datAgendamentoEnvio;
	}

	public void setDatAgendamentoEnvio(Timestamp datAgendamentoEnvio) {
		this.datAgendamentoEnvio = datAgendamentoEnvio;
	}

	public Timestamp getDatSolicitacao() {
		return this.datSolicitacao;
	}

	public void setDatSolicitacao(Timestamp datSolicitacao) {
		this.datSolicitacao = datSolicitacao;
	}

	public String getEndEmail() {
		return this.endEmail;
	}

	public void setEndEmail(String endEmail) {
		this.endEmail = endEmail;
	}

	public String getIdCliente() {
		return this.idCliente;
	}

	public void setIdCliente(String idCliente) {
		this.idCliente = idCliente;
	}

	public String getIdFuncionario() {
		return this.idFuncionario;
	}

	public void setIdFuncionario(String idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public String getIdResponsavel() {
		return this.idResponsavel;
	}

	public void setIdResponsavel(String idResponsavel) {
		this.idResponsavel = idResponsavel;
	}

	public String getIdSistemaSolicitador() {
		return this.idSistemaSolicitador;
	}

	public void setIdSistemaSolicitador(String idSistemaSolicitador) {
		this.idSistemaSolicitador = idSistemaSolicitador;
	}

	public String getNumTelefoneMovel() {
		return this.numTelefoneMovel;
	}

	public void setNumTelefoneMovel(String numTelefoneMovel) {
		this.numTelefoneMovel = numTelefoneMovel;
	}

	public BigDecimal getValTransacao() {
		return this.valTransacao;
	}

	public void setValTransacao(BigDecimal valTransacao) {
		this.valTransacao = valTransacao;
	}

	public MensagemNotificacao getMensagemNotificacao() {
		return this.mensagemNotificacao;
	}

	public void setMensagemNotificacao(MensagemNotificacao mensagemNotificacao) {
		this.mensagemNotificacao = mensagemNotificacao;
	}

	public List<SolicitacaoMensagemNotificacao> getSolicitacaoMensagemNotificacaos() {
		return this.solicitacaoMensagemNotificacaos;
	}

	public void setSolicitacaoMensagemNotificacaos(List<SolicitacaoMensagemNotificacao> solicitacaoMensagemNotificacaos) {
		this.solicitacaoMensagemNotificacaos = solicitacaoMensagemNotificacaos;
	}

	public SolicitacaoMensagemNotificacao addSolicitacaoMensagemNotificacao(SolicitacaoMensagemNotificacao solicitacaoMensagemNotificacao) {
		getSolicitacaoMensagemNotificacaos().add(solicitacaoMensagemNotificacao);
		solicitacaoMensagemNotificacao.setSolicitacaoNotificacao(this);

		return solicitacaoMensagemNotificacao;
	}

	public SolicitacaoMensagemNotificacao removeSolicitacaoMensagemNotificacao(SolicitacaoMensagemNotificacao solicitacaoMensagemNotificacao) {
		getSolicitacaoMensagemNotificacaos().remove(solicitacaoMensagemNotificacao);
		solicitacaoMensagemNotificacao.setSolicitacaoNotificacao(null);

		return solicitacaoMensagemNotificacao;
	}

	public ArquivoFornecedorMensagem getArquivoFornecedorMensagem() {
		return this.arquivoFornecedorMensagem;
	}

	public void setArquivoFornecedorMensagem(ArquivoFornecedorMensagem arquivoFornecedorMensagem) {
		this.arquivoFornecedorMensagem = arquivoFornecedorMensagem;
	}

	public ArquivoSolicitacaoNotificacao getArquivoSolicitacaoNotificacao() {
		return this.arquivoSolicitacaoNotificacao;
	}

	public void setArquivoSolicitacaoNotificacao(ArquivoSolicitacaoNotificacao arquivoSolicitacaoNotificacao) {
		this.arquivoSolicitacaoNotificacao = arquivoSolicitacaoNotificacao;
	}

	public ConfiguracaoNotificacao getConfiguracaoNotificacao() {
		return this.configuracaoNotificacao;
	}

	public void setConfiguracaoNotificacao(ConfiguracaoNotificacao configuracaoNotificacao) {
		this.configuracaoNotificacao = configuracaoNotificacao;
	}

	public Evento getEvento() {
		return this.evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public Notificacao getNotificacao() {
		return this.notificacao;
	}

	public void setNotificacao(Notificacao notificacao) {
		this.notificacao = notificacao;
	}

	public TransactionReason getTransactionReason() {
		return this.transactionReason;
	}

	public void setTransactionReason(TransactionReason transactionReason) {
		this.transactionReason = transactionReason;
	}

}