package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.ModeloMensagem;

public class ModeloMensagemDAO extends GenericDAO<ModeloMensagem> {

	public ModeloMensagemDAO() {
		super(ModeloMensagem.class);
	}

}