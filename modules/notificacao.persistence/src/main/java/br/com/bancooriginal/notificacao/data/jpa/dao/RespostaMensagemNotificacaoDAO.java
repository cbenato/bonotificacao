package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.RespostaMensagemNotificacao;

public class RespostaMensagemNotificacaoDAO extends GenericDAO<RespostaMensagemNotificacao> {

	public RespostaMensagemNotificacaoDAO() {
		super(RespostaMensagemNotificacao.class);
	}

}
