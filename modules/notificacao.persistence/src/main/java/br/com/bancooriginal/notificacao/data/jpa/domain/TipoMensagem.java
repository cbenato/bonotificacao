package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the TIPO_MENSAGEM database table.
 * 
 */
@Entity
@Table(name="TIPO_MENSAGEM")
@NamedQuery(name="TipoMensagem.findAll", query="SELECT t FROM TipoMensagem t")
public class TipoMensagem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_TIPO_MENSAGEM")
	private long codTipoMensagem;

	@Column(name="DES_TIPO_MENSAGEM")
	private String desTipoMensagem;

	@Column(name="IND_SEGURANCA")
	private String indSeguranca;

	@Column(name="NOM_TIPO_MENSAGEM")
	private String nomTipoMensagem;
	
	@Column(name="ID_TIPO_MENSAGEM")
	private long idTipoMensagem;
	
	//bi-directional many-to-one association to FormaEnvioNotificacao
	@ManyToOne
	@JoinColumn(name="ID_FORMA_ENVIO_NOTIFICACAO", referencedColumnName="ID_FORMA_ENVIO_NOTIFICACAO")
	private FormaEnvioNotificacao formaEnvioNotificacao;

	//bi-directional many-to-one association to ModeloMensagem
	@OneToMany(mappedBy="tipoMensagem")
	private List<ModeloMensagem> modeloMensagems;

	public TipoMensagem() {
	}

	public long getCodTipoMensagem() {
		return this.codTipoMensagem;
	}

	public void setCodTipoMensagem(long codTipoMensagem) {
		this.codTipoMensagem = codTipoMensagem;
	}

	public String getDesTipoMensagem() {
		return this.desTipoMensagem;
	}

	public void setDesTipoMensagem(String desTipoMensagem) {
		this.desTipoMensagem = desTipoMensagem;
	}

	public String getIndSeguranca() {
		return this.indSeguranca;
	}

	public void setIndSeguranca(String indSeguranca) {
		this.indSeguranca = indSeguranca;
	}

	public String getNomTipoMensagem() {
		return this.nomTipoMensagem;
	}

	public void setNomTipoMensagem(String nomTipoMensagem) {
		this.nomTipoMensagem = nomTipoMensagem;
	}

	public FormaEnvioNotificacao getFormaEnvioNotificacao() {
		return this.formaEnvioNotificacao;
	}

	public void setFormaEnvioNotificacao(FormaEnvioNotificacao formaEnvioNotificacao) {
		this.formaEnvioNotificacao = formaEnvioNotificacao;
	}

	public List<ModeloMensagem> getModeloMensagems() {
		return this.modeloMensagems;
	}

	public void setModeloMensagems(List<ModeloMensagem> modeloMensagems) {
		this.modeloMensagems = modeloMensagems;
	}

	public ModeloMensagem addModeloMensagem(ModeloMensagem modeloMensagem) {
		getModeloMensagems().add(modeloMensagem);
		modeloMensagem.setTipoMensagem(this);

		return modeloMensagem;
	}

	public ModeloMensagem removeModeloMensagem(ModeloMensagem modeloMensagem) {
		getModeloMensagems().remove(modeloMensagem);
		modeloMensagem.setTipoMensagem(null);

		return modeloMensagem;
	}

}