package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.SolicitacaoMensagemNotificacao;

public class SolicitacaoMensagemNotificacaoDAO extends GenericDAO<SolicitacaoMensagemNotificacao> {

	public SolicitacaoMensagemNotificacaoDAO() {
		super(SolicitacaoMensagemNotificacao.class);
	}

}
