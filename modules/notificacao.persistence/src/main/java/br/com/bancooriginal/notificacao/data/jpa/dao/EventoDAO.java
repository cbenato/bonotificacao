package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.Evento;


public class EventoDAO extends GenericDAO<Evento> {

	public EventoDAO() {
		super(Evento.class);
	}

}