package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the MODELO_MENSAGEM database table.
 * 
 */
@Entity
@Table(name="MODELO_MENSAGEM")
@NamedQuery(name="ModeloMensagem.findAll", query="SELECT m FROM ModeloMensagem m")
public class ModeloMensagem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_MODELO_MENSAGEM")
	private long codModeloMensagem;

	@Column(name="DES_MODELO_MENSAGEM")
	private String desModeloMensagem;

	@Column(name="NOM_MODELO_MENSAGEM")
	private String nomModeloMensagem;

	@Column(name="TIPO_MODELO_MENSAGEM")
	private String tipoModeloMensagem;
	
	@Column(name="ID_MODELO_MENSAGEM")
	private long idModeloMensagem;
	

	//bi-directional many-to-one association to ParametroModelo
	@OneToMany(mappedBy="modeloMensagem")
	private List<ParametroModelo> parametroModelos;

	//bi-directional many-to-one association to Mensagem
	@OneToMany(mappedBy="modeloMensagem")
	private List<Mensagem> mensagems;

	//bi-directional many-to-one association to TipoMensagem
	@ManyToOne
	@JoinColumn(name="ID_TIPO_MENSAGEM", referencedColumnName="ID_TIPO_MENSAGEM")
	private TipoMensagem tipoMensagem;

	public ModeloMensagem() {
	}

	public long getCodModeloMensagem() {
		return this.codModeloMensagem;
	}

	public void setCodModeloMensagem(long codModeloMensagem) {
		this.codModeloMensagem = codModeloMensagem;
	}

	public String getDesModeloMensagem() {
		return this.desModeloMensagem;
	}

	public void setDesModeloMensagem(String desModeloMensagem) {
		this.desModeloMensagem = desModeloMensagem;
	}

	public String getNomModeloMensagem() {
		return this.nomModeloMensagem;
	}

	public void setNomModeloMensagem(String nomModeloMensagem) {
		this.nomModeloMensagem = nomModeloMensagem;
	}

	public String getTipoModeloMensagem() {
		return this.tipoModeloMensagem;
	}

	public void setTipoModeloMensagem(String tipoModeloMensagem) {
		this.tipoModeloMensagem = tipoModeloMensagem;
	}

	public List<ParametroModelo> getParametroModelos() {
		return this.parametroModelos;
	}

	public void setParametroModelos(List<ParametroModelo> parametroModelos) {
		this.parametroModelos = parametroModelos;
	}

	public ParametroModelo addParametroModelo(ParametroModelo parametroModelo) {
		getParametroModelos().add(parametroModelo);
		parametroModelo.setModeloMensagem(this);

		return parametroModelo;
	}

	public ParametroModelo removeParametroModelo(ParametroModelo parametroModelo) {
		getParametroModelos().remove(parametroModelo);
		parametroModelo.setModeloMensagem(null);

		return parametroModelo;
	}

	public List<Mensagem> getMensagems() {
		return this.mensagems;
	}

	public void setMensagems(List<Mensagem> mensagems) {
		this.mensagems = mensagems;
	}

	public Mensagem addMensagem(Mensagem mensagem) {
		getMensagems().add(mensagem);
		mensagem.setModeloMensagem(this);

		return mensagem;
	}

	public Mensagem removeMensagem(Mensagem mensagem) {
		getMensagems().remove(mensagem);
		mensagem.setModeloMensagem(null);

		return mensagem;
	}

	public TipoMensagem getTipoMensagem() {
		return this.tipoMensagem;
	}

	public void setTipoMensagem(TipoMensagem tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}

}