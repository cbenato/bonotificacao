package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.TransactionReason;

public class TransactionReasonDAO extends GenericDAO<TransactionReason> {

	public TransactionReasonDAO() {
		super(TransactionReason.class);
	}

}