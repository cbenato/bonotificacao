package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the MODELO_MENSAGEM_LONGA database table.
 * 
 */
@Entity
@Table(name="MODELO_MENSAGEM_LONGA")
@NamedQuery(name="ModeloMensagemLonga.findAll", query="SELECT m FROM ModeloMensagemLonga m")
public class ModeloMensagemLonga implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_MODELO_MENSAGEM")
	private long idModeloMensagem;

	@Column(name="CONTD_MODELO_MENSAGEM")
	private String contdModeloMensagem;

	public ModeloMensagemLonga() {
	}

	public long getIdModeloMensagem() {
		return this.idModeloMensagem;
	}

	public void setIdModeloMensagem(long idModeloMensagem) {
		this.idModeloMensagem = idModeloMensagem;
	}

	public String getContdModeloMensagem() {
		return this.contdModeloMensagem;
	}

	public void setContdModeloMensagem(String contdModeloMensagem) {
		this.contdModeloMensagem = contdModeloMensagem;
	}

}