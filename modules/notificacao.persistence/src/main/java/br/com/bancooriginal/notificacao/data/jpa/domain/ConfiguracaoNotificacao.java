package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the CONFIGURACAO_NOTIFICACAO database table.
 * 
 */
@Entity
@Table(name="CONFIGURACAO_NOTIFICACAO")
@NamedQuery(name="ConfiguracaoNotificacao.findAll", query="SELECT c FROM ConfiguracaoNotificacao c")
public class ConfiguracaoNotificacao implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_CONFIGURACAO_NOTIFICACAO")
	private long codConfiguracaoNotificacao;

	@Column(name="CHAVE_PRODUTO")
	private String chaveProduto;

	@Column(name="COD_PRODUTO")
	private BigDecimal codProduto;

	@Column(name="COD_SITUACAO")
	private BigDecimal codSituacao;

	@Column(name="DAT_ALTERACAO")
	private Timestamp datAlteracao;

	@Column(name="DAT_INCLUSAO")
	private Timestamp datInclusao;

	@Column(name="ID_FORMA_ENVIO")
	private BigDecimal idFormaEnvio;

	@Column(name="ID_GRUPO_TRANSACAO")
	private BigDecimal idGrupoTransacao;

	@Column(name="ID_TIPO_CONFIGURACAO")
	private String idTipoConfiguracao;

	//bi-directional many-to-one association to Evento
	@ManyToOne
	@JoinColumn(name="ID_EVENTO", referencedColumnName="ID_EVENTO")
	private Evento evento;

	//bi-directional many-to-one association to FormaEnvioNotificacao
	@ManyToOne
	@JoinColumn(name="ID_FORMA_ENVIO_NOTIFICACAO", referencedColumnName="ID_FORMA_ENVIO_NOTIFICACAO")
	private FormaEnvioNotificacao formaEnvioNotificacao;

	//bi-directional many-to-one association to ParametrizacaoNotificacao
	@ManyToOne
	@JoinColumn(name="COD_PARAMETRIZACAO_NOTIFICACAO")
	private ParametrizacaoNotificacao parametrizacaoNotificacao;

	//bi-directional many-to-one association to SolicitacaoNotificacao
	@OneToMany(mappedBy="configuracaoNotificacao")
	private List<SolicitacaoNotificacao> solicitacaoNotificacaos;

	public ConfiguracaoNotificacao() {
	}

	public long getCodConfiguracaoNotificacao() {
		return this.codConfiguracaoNotificacao;
	}

	public void setCodConfiguracaoNotificacao(long codConfiguracaoNotificacao) {
		this.codConfiguracaoNotificacao = codConfiguracaoNotificacao;
	}

	public String getChaveProduto() {
		return this.chaveProduto;
	}

	public void setChaveProduto(String chaveProduto) {
		this.chaveProduto = chaveProduto;
	}

	public BigDecimal getCodProduto() {
		return this.codProduto;
	}

	public void setCodProduto(BigDecimal codProduto) {
		this.codProduto = codProduto;
	}

	public BigDecimal getCodSituacao() {
		return this.codSituacao;
	}

	public void setCodSituacao(BigDecimal codSituacao) {
		this.codSituacao = codSituacao;
	}

	public Timestamp getDatAlteracao() {
		return this.datAlteracao;
	}

	public void setDatAlteracao(Timestamp datAlteracao) {
		this.datAlteracao = datAlteracao;
	}

	public Timestamp getDatInclusao() {
		return this.datInclusao;
	}

	public void setDatInclusao(Timestamp datInclusao) {
		this.datInclusao = datInclusao;
	}

	public BigDecimal getIdFormaEnvio() {
		return this.idFormaEnvio;
	}

	public void setIdFormaEnvio(BigDecimal idFormaEnvio) {
		this.idFormaEnvio = idFormaEnvio;
	}

	public BigDecimal getIdGrupoTransacao() {
		return this.idGrupoTransacao;
	}

	public void setIdGrupoTransacao(BigDecimal idGrupoTransacao) {
		this.idGrupoTransacao = idGrupoTransacao;
	}

	public String getIdTipoConfiguracao() {
		return this.idTipoConfiguracao;
	}

	public void setIdTipoConfiguracao(String idTipoConfiguracao) {
		this.idTipoConfiguracao = idTipoConfiguracao;
	}

	public Evento getEvento() {
		return this.evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public FormaEnvioNotificacao getFormaEnvioNotificacao() {
		return this.formaEnvioNotificacao;
	}

	public void setFormaEnvioNotificacao(FormaEnvioNotificacao formaEnvioNotificacao) {
		this.formaEnvioNotificacao = formaEnvioNotificacao;
	}

	public ParametrizacaoNotificacao getParametrizacaoNotificacao() {
		return this.parametrizacaoNotificacao;
	}

	public void setParametrizacaoNotificacao(ParametrizacaoNotificacao parametrizacaoNotificacao) {
		this.parametrizacaoNotificacao = parametrizacaoNotificacao;
	}

	public List<SolicitacaoNotificacao> getSolicitacaoNotificacaos() {
		return this.solicitacaoNotificacaos;
	}

	public void setSolicitacaoNotificacaos(List<SolicitacaoNotificacao> solicitacaoNotificacaos) {
		this.solicitacaoNotificacaos = solicitacaoNotificacaos;
	}

	public SolicitacaoNotificacao addSolicitacaoNotificacao(SolicitacaoNotificacao solicitacaoNotificacao) {
		getSolicitacaoNotificacaos().add(solicitacaoNotificacao);
		solicitacaoNotificacao.setConfiguracaoNotificacao(this);

		return solicitacaoNotificacao;
	}

	public SolicitacaoNotificacao removeSolicitacaoNotificacao(SolicitacaoNotificacao solicitacaoNotificacao) {
		getSolicitacaoNotificacaos().remove(solicitacaoNotificacao);
		solicitacaoNotificacao.setConfiguracaoNotificacao(null);

		return solicitacaoNotificacao;
	}

}