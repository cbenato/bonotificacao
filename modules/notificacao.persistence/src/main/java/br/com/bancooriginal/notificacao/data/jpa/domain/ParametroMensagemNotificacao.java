package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PARAMETRO_MENSAGEM_NOTIFIACAO database table.
 * 
 */
@Entity
@Table(name="PARAMETRO_MENSAGEM_NOTIFICACAO")
@NamedQuery(name="ParametroMensagemNotifiacao.findAll", query="SELECT p FROM ParametroMensagemNotificacao p")
public class ParametroMensagemNotificacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ParametroMensagemNotificacaoPK id;

	@Column(name="COD_SISTEMA_SOLICITADOR")
	private String codSistemaSolicitador;

	@Column(name="VAL_PARAMETRO")
	private String valParametro;

	//bi-directional many-to-one association to MensagemNotificacao
	@ManyToOne
	@JoinColumn(name="COD_SOLICITACAO_NOTIFICACAO", insertable=false, updatable=false)
	private MensagemNotificacao mensagemNotificacao;

	//bi-directional many-to-one association to ParametroSolicitacao
	@ManyToOne
	@JoinColumn(name="ID_PARAMETRO_MENSAGEM", insertable=false, updatable=false)
	private ParametroSolicitacao parametroSolicitacao;

	public ParametroMensagemNotificacao() {
	}

	public ParametroMensagemNotificacaoPK getId() {
		return this.id;
	}

	public void setId(ParametroMensagemNotificacaoPK id) {
		this.id = id;
	}

	public String getCodSistemaSolicitador() {
		return this.codSistemaSolicitador;
	}

	public void setCodSistemaSolicitador(String codSistemaSolicitador) {
		this.codSistemaSolicitador = codSistemaSolicitador;
	}

	public String getValParametro() {
		return this.valParametro;
	}

	public void setValParametro(String valParametro) {
		this.valParametro = valParametro;
	}

	public MensagemNotificacao getMensagemNotificacao() {
		return this.mensagemNotificacao;
	}

	public void setMensagemNotificacao(MensagemNotificacao mensagemNotificacao) {
		this.mensagemNotificacao = mensagemNotificacao;
	}

	public ParametroSolicitacao getParametroSolicitacao() {
		return this.parametroSolicitacao;
	}

	public void setParametroSolicitacao(ParametroSolicitacao parametroSolicitacao) {
		this.parametroSolicitacao = parametroSolicitacao;
	}

}