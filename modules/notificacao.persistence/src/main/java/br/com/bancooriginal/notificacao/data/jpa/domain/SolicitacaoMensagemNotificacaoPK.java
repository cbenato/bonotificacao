package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the SOLICITACAO_MENSAGEM_NOTIFICACAO database table.
 * 
 */
@Embeddable
public class SolicitacaoMensagemNotificacaoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="COD_SOLICITACAO_MENSAGEM")
	private long codSolicitacaoMensagem;

	@Column(name="COD_SOLICITACAO_NOTIFICACAO", insertable=false, updatable=false)
	private long codSolicitacaoNotificacao;

	public SolicitacaoMensagemNotificacaoPK() {
	}
	public long getCodSolicitacaoMensagem() {
		return this.codSolicitacaoMensagem;
	}
	public void setCodSolicitacaoMensagem(long codSolicitacaoMensagem) {
		this.codSolicitacaoMensagem = codSolicitacaoMensagem;
	}
	public long getCodSolicitacaoNotificacao() {
		return this.codSolicitacaoNotificacao;
	}
	public void setCodSolicitacaoNotificacao(long codSolicitacaoNotificacao) {
		this.codSolicitacaoNotificacao = codSolicitacaoNotificacao;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SolicitacaoMensagemNotificacaoPK)) {
			return false;
		}
		SolicitacaoMensagemNotificacaoPK castOther = (SolicitacaoMensagemNotificacaoPK)other;
		return 
			(this.codSolicitacaoMensagem == castOther.codSolicitacaoMensagem)
			&& (this.codSolicitacaoNotificacao == castOther.codSolicitacaoNotificacao);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.codSolicitacaoMensagem ^ (this.codSolicitacaoMensagem >>> 32)));
		hash = hash * prime + ((int) (this.codSolicitacaoNotificacao ^ (this.codSolicitacaoNotificacao >>> 32)));
		
		return hash;
	}
}