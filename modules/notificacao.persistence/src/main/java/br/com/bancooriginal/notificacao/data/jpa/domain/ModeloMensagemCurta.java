package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the MODELO_MENSAGEM_CURTA database table.
 * 
 */
@Entity
@Table(name="MODELO_MENSAGEM_CURTA")
@NamedQuery(name="ModeloMensagemCurta.findAll", query="SELECT m FROM ModeloMensagemCurta m")
public class ModeloMensagemCurta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_MODELO_MENSAGEM")
	private long idModeloMensagem;

	@Column(name="CONTD_MODELO_MENSAGEM")
	private String contdModeloMensagem;

	public ModeloMensagemCurta() {
	}

	public long getIdModeloMensagem() {
		return this.idModeloMensagem;
	}

	public void setIdModeloMensagem(long idModeloMensagem) {
		this.idModeloMensagem = idModeloMensagem;
	}

	public String getContdModeloMensagem() {
		return this.contdModeloMensagem;
	}

	public void setContdModeloMensagem(String contdModeloMensagem) {
		this.contdModeloMensagem = contdModeloMensagem;
	}

}