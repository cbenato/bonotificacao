package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.SituacaoFornecedorMensagem;

public class SituacaoFornecedorMensagemDAO extends GenericDAO<SituacaoFornecedorMensagem> {

	public SituacaoFornecedorMensagemDAO() {
		super(SituacaoFornecedorMensagem.class);
	}

}
