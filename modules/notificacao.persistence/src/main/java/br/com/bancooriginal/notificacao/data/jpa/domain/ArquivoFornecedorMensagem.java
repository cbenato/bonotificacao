package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the ARQUIVO_FORNECEDOR_MENSAGEM database table.
 * 
 */
@Entity
@Table(name="ARQUIVO_FORNECEDOR_MENSAGEM")
@NamedQuery(name="ArquivoFornecedorMensagem.findAll", query="SELECT a FROM ArquivoFornecedorMensagem a")
public class ArquivoFornecedorMensagem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_ARQUIVO_FORNECEDOR")
	private long codArquivoFornecedor;

	@Column(name="NOM_ARQUIVO_FORNECEDOR")
	private String nomArquivoFornecedor;

	//bi-directional many-to-one association to FornecedorMensagem
	@ManyToOne
	@JoinColumn(name="COD_FORNECEDOR")
	private FornecedorMensagem fornecedorMensagem;

	//bi-directional many-to-one association to EnvioArquivoFornecedorMensagem
	@OneToMany(mappedBy="arquivoFornecedorMensagem")
	private List<EnvioArquivoFornecedorMensagem> envioArquivoFornecedorMensagems;

	//bi-directional many-to-one association to SolicitacaoNotificacao
	@OneToMany(mappedBy="arquivoFornecedorMensagem")
	private List<SolicitacaoNotificacao> solicitacaoNotificacaos;

	public ArquivoFornecedorMensagem() {
	}

	public long getCodArquivoFornecedor() {
		return this.codArquivoFornecedor;
	}

	public void setCodArquivoFornecedor(long codArquivoFornecedor) {
		this.codArquivoFornecedor = codArquivoFornecedor;
	}

	public String getNomArquivoFornecedor() {
		return this.nomArquivoFornecedor;
	}

	public void setNomArquivoFornecedor(String nomArquivoFornecedor) {
		this.nomArquivoFornecedor = nomArquivoFornecedor;
	}

	public FornecedorMensagem getFornecedorMensagem() {
		return this.fornecedorMensagem;
	}

	public void setFornecedorMensagem(FornecedorMensagem fornecedorMensagem) {
		this.fornecedorMensagem = fornecedorMensagem;
	}

	public List<EnvioArquivoFornecedorMensagem> getEnvioArquivoFornecedorMensagems() {
		return this.envioArquivoFornecedorMensagems;
	}

	public void setEnvioArquivoFornecedorMensagems(List<EnvioArquivoFornecedorMensagem> envioArquivoFornecedorMensagems) {
		this.envioArquivoFornecedorMensagems = envioArquivoFornecedorMensagems;
	}

	public EnvioArquivoFornecedorMensagem addEnvioArquivoFornecedorMensagem(EnvioArquivoFornecedorMensagem envioArquivoFornecedorMensagem) {
		getEnvioArquivoFornecedorMensagems().add(envioArquivoFornecedorMensagem);
		envioArquivoFornecedorMensagem.setArquivoFornecedorMensagem(this);

		return envioArquivoFornecedorMensagem;
	}

	public EnvioArquivoFornecedorMensagem removeEnvioArquivoFornecedorMensagem(EnvioArquivoFornecedorMensagem envioArquivoFornecedorMensagem) {
		getEnvioArquivoFornecedorMensagems().remove(envioArquivoFornecedorMensagem);
		envioArquivoFornecedorMensagem.setArquivoFornecedorMensagem(null);

		return envioArquivoFornecedorMensagem;
	}

	public List<SolicitacaoNotificacao> getSolicitacaoNotificacaos() {
		return this.solicitacaoNotificacaos;
	}

	public void setSolicitacaoNotificacaos(List<SolicitacaoNotificacao> solicitacaoNotificacaos) {
		this.solicitacaoNotificacaos = solicitacaoNotificacaos;
	}

	public SolicitacaoNotificacao addSolicitacaoNotificacao(SolicitacaoNotificacao solicitacaoNotificacao) {
		getSolicitacaoNotificacaos().add(solicitacaoNotificacao);
		solicitacaoNotificacao.setArquivoFornecedorMensagem(this);

		return solicitacaoNotificacao;
	}

	public SolicitacaoNotificacao removeSolicitacaoNotificacao(SolicitacaoNotificacao solicitacaoNotificacao) {
		getSolicitacaoNotificacaos().remove(solicitacaoNotificacao);
		solicitacaoNotificacao.setArquivoFornecedorMensagem(null);

		return solicitacaoNotificacao;
	}

}