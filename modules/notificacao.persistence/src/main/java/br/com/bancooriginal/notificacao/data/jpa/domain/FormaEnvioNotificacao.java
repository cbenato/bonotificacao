package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the FORMA_ENVIO_NOTIFICACAO database table.
 * 
 */
@Entity
@Table(name="FORMA_ENVIO_NOTIFICACAO")
@NamedQuery(name="FormaEnvioNotificacao.findAll", query="SELECT f FROM FormaEnvioNotificacao f")
public class FormaEnvioNotificacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_FORMA_ENVIO_NOTIFICACAO")
	private long codFormaEnvioNotificacao;

	@Column(name="DES_FORMA_ENVIO_NOTIFICACAO")
	private String desFormaEnvioNotificacao;

	@Column(name="NOM_FORMA_ENVIO_NOTIFICACAO")
	private String nomFormaEnvioNotificacao;

	@Column(name="ID_FORMA_ENVIO_NOTIFICACAO")
	private String idFormaEnvioNotificacao;
	
	//bi-directional many-to-one association to SituacaoFornecedorMensagem
	@OneToMany(mappedBy="formaEnvioNotificacao")
	private List<SituacaoFornecedorMensagem> situacaoFornecedorMensagems;

	
	//bi-directional many-to-one association to TipoMensagem
	@OneToMany(mappedBy="formaEnvioNotificacao")
	private List<TipoMensagem> tipoMensagems;

	//bi-directional many-to-one association to ConfiguracaoNotificacao
	@OneToMany(mappedBy="formaEnvioNotificacao")
	private List<ConfiguracaoNotificacao> configuracaoNotificacaos;

	//bi-directional many-to-one association to EnvioArquivoFornecedorMensagem
	@OneToMany(mappedBy="formaEnvioNotificacao")
	private List<EnvioArquivoFornecedorMensagem> envioArquivoFornecedorMensagems;

	//bi-directional many-to-one association to FornecedorFormaEnvioNotificacao
	@OneToMany(mappedBy="formaEnvioNotificacao")
	private List<FornecedorFormaEnvioNotificacao> fornecedorFormaEnvioNotificacaos;

	//bi-directional many-to-one association to ParametrizacaoMensagemNotificacao
	@OneToMany(mappedBy="formaEnvioNotificacao")
	private List<ParametrizacaoMensagemNotificacao> parametrizacaoMensagemNotificacaos;

	public FormaEnvioNotificacao() {
	}

	public long getCodFormaEnvioNotificacao() {
		return this.codFormaEnvioNotificacao;
	}

	public void setCodFormaEnvioNotificacao(long codFormaEnvioNotificacao) {
		this.codFormaEnvioNotificacao = codFormaEnvioNotificacao;
	}

	public String getDesFormaEnvioNotificacao() {
		return this.desFormaEnvioNotificacao;
	}

	public void setDesFormaEnvioNotificacao(String desFormaEnvioNotificacao) {
		this.desFormaEnvioNotificacao = desFormaEnvioNotificacao;
	}

	public String getNomFormaEnvioNotificacao() {
		return this.nomFormaEnvioNotificacao;
	}

	public void setNomFormaEnvioNotificacao(String nomFormaEnvioNotificacao) {
		this.nomFormaEnvioNotificacao = nomFormaEnvioNotificacao;
	}

	public List<SituacaoFornecedorMensagem> getSituacaoFornecedorMensagems() {
		return this.situacaoFornecedorMensagems;
	}

	public void setSituacaoFornecedorMensagems(List<SituacaoFornecedorMensagem> situacaoFornecedorMensagems) {
		this.situacaoFornecedorMensagems = situacaoFornecedorMensagems;
	}

	public SituacaoFornecedorMensagem addSituacaoFornecedorMensagem(SituacaoFornecedorMensagem situacaoFornecedorMensagem) {
		getSituacaoFornecedorMensagems().add(situacaoFornecedorMensagem);
		situacaoFornecedorMensagem.setFormaEnvioNotificacao(this);

		return situacaoFornecedorMensagem;
	}

	public SituacaoFornecedorMensagem removeSituacaoFornecedorMensagem(SituacaoFornecedorMensagem situacaoFornecedorMensagem) {
		getSituacaoFornecedorMensagems().remove(situacaoFornecedorMensagem);
		situacaoFornecedorMensagem.setFormaEnvioNotificacao(null);

		return situacaoFornecedorMensagem;
	}

	public List<TipoMensagem> getTipoMensagems() {
		return this.tipoMensagems;
	}

	public void setTipoMensagems(List<TipoMensagem> tipoMensagems) {
		this.tipoMensagems = tipoMensagems;
	}

	public TipoMensagem addTipoMensagem(TipoMensagem tipoMensagem) {
		getTipoMensagems().add(tipoMensagem);
		tipoMensagem.setFormaEnvioNotificacao(this);

		return tipoMensagem;
	}

	public TipoMensagem removeTipoMensagem(TipoMensagem tipoMensagem) {
		getTipoMensagems().remove(tipoMensagem);
		tipoMensagem.setFormaEnvioNotificacao(null);

		return tipoMensagem;
	}

	public List<ConfiguracaoNotificacao> getConfiguracaoNotificacaos() {
		return this.configuracaoNotificacaos;
	}

	public void setConfiguracaoNotificacaos(List<ConfiguracaoNotificacao> configuracaoNotificacaos) {
		this.configuracaoNotificacaos = configuracaoNotificacaos;
	}

	public ConfiguracaoNotificacao addConfiguracaoNotificacao(ConfiguracaoNotificacao configuracaoNotificacao) {
		getConfiguracaoNotificacaos().add(configuracaoNotificacao);
		configuracaoNotificacao.setFormaEnvioNotificacao(this);

		return configuracaoNotificacao;
	}

	public ConfiguracaoNotificacao removeConfiguracaoNotificacao(ConfiguracaoNotificacao configuracaoNotificacao) {
		getConfiguracaoNotificacaos().remove(configuracaoNotificacao);
		configuracaoNotificacao.setFormaEnvioNotificacao(null);

		return configuracaoNotificacao;
	}

	public List<EnvioArquivoFornecedorMensagem> getEnvioArquivoFornecedorMensagems() {
		return this.envioArquivoFornecedorMensagems;
	}

	public void setEnvioArquivoFornecedorMensagems(List<EnvioArquivoFornecedorMensagem> envioArquivoFornecedorMensagems) {
		this.envioArquivoFornecedorMensagems = envioArquivoFornecedorMensagems;
	}

	public EnvioArquivoFornecedorMensagem addEnvioArquivoFornecedorMensagem(EnvioArquivoFornecedorMensagem envioArquivoFornecedorMensagem) {
		getEnvioArquivoFornecedorMensagems().add(envioArquivoFornecedorMensagem);
		envioArquivoFornecedorMensagem.setFormaEnvioNotificacao(this);

		return envioArquivoFornecedorMensagem;
	}

	public EnvioArquivoFornecedorMensagem removeEnvioArquivoFornecedorMensagem(EnvioArquivoFornecedorMensagem envioArquivoFornecedorMensagem) {
		getEnvioArquivoFornecedorMensagems().remove(envioArquivoFornecedorMensagem);
		envioArquivoFornecedorMensagem.setFormaEnvioNotificacao(null);

		return envioArquivoFornecedorMensagem;
	}

	public List<FornecedorFormaEnvioNotificacao> getFornecedorFormaEnvioNotificacaos() {
		return this.fornecedorFormaEnvioNotificacaos;
	}

	public void setFornecedorFormaEnvioNotificacaos(List<FornecedorFormaEnvioNotificacao> fornecedorFormaEnvioNotificacaos) {
		this.fornecedorFormaEnvioNotificacaos = fornecedorFormaEnvioNotificacaos;
	}

	public FornecedorFormaEnvioNotificacao addFornecedorFormaEnvioNotificacao(FornecedorFormaEnvioNotificacao fornecedorFormaEnvioNotificacao) {
		getFornecedorFormaEnvioNotificacaos().add(fornecedorFormaEnvioNotificacao);
		fornecedorFormaEnvioNotificacao.setFormaEnvioNotificacao(this);

		return fornecedorFormaEnvioNotificacao;
	}

	public FornecedorFormaEnvioNotificacao removeFornecedorFormaEnvioNotificacao(FornecedorFormaEnvioNotificacao fornecedorFormaEnvioNotificacao) {
		getFornecedorFormaEnvioNotificacaos().remove(fornecedorFormaEnvioNotificacao);
		fornecedorFormaEnvioNotificacao.setFormaEnvioNotificacao(null);

		return fornecedorFormaEnvioNotificacao;
	}

	public List<ParametrizacaoMensagemNotificacao> getParametrizacaoMensagemNotificacaos() {
		return this.parametrizacaoMensagemNotificacaos;
	}

	public void setParametrizacaoMensagemNotificacaos(List<ParametrizacaoMensagemNotificacao> parametrizacaoMensagemNotificacaos) {
		this.parametrizacaoMensagemNotificacaos = parametrizacaoMensagemNotificacaos;
	}

	public ParametrizacaoMensagemNotificacao addParametrizacaoMensagemNotificacao(ParametrizacaoMensagemNotificacao parametrizacaoMensagemNotificacao) {
		getParametrizacaoMensagemNotificacaos().add(parametrizacaoMensagemNotificacao);
		parametrizacaoMensagemNotificacao.setFormaEnvioNotificacao(this);

		return parametrizacaoMensagemNotificacao;
	}

	public ParametrizacaoMensagemNotificacao removeParametrizacaoMensagemNotificacao(ParametrizacaoMensagemNotificacao parametrizacaoMensagemNotificacao) {
		getParametrizacaoMensagemNotificacaos().remove(parametrizacaoMensagemNotificacao);
		parametrizacaoMensagemNotificacao.setFormaEnvioNotificacao(null);

		return parametrizacaoMensagemNotificacao;
	}

}