package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the TRANSACAO_EVENTO database table.
 * 
 */
@Entity
@Table(name="TRANSACAO_EVENTO")
@NamedQuery(name="TransacaoEvento.findAll", query="SELECT t FROM TransacaoEvento t")
public class TransacaoEvento implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TransacaoEventoPK id;

	@Column(name="ID_EVENTO")
	private BigDecimal idEvento;

	public TransacaoEvento() {
	}

	public TransacaoEventoPK getId() {
		return this.id;
	}

	public void setId(TransacaoEventoPK id) {
		this.id = id;
	}

	public BigDecimal getIdEvento() {
		return this.idEvento;
	}

	public void setIdEvento(BigDecimal idEvento) {
		this.idEvento = idEvento;
	}

}