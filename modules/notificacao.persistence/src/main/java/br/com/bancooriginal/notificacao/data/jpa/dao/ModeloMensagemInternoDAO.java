package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.ModeloMensagemInterno;

public class ModeloMensagemInternoDAO extends GenericDAO<ModeloMensagemInterno> {

	public ModeloMensagemInternoDAO() {
		super(ModeloMensagemInterno.class);
	}

}