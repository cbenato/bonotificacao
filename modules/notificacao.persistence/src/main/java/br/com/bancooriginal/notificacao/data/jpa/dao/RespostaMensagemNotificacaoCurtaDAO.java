package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.RespostaMensagemNotificacaoCurta;

public class RespostaMensagemNotificacaoCurtaDAO extends GenericDAO<RespostaMensagemNotificacaoCurta> {

	public RespostaMensagemNotificacaoCurtaDAO() {
		super(RespostaMensagemNotificacaoCurta.class);
	}

}
