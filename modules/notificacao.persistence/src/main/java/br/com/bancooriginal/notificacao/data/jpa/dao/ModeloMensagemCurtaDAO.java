package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.ModeloMensagemCurta;

public class ModeloMensagemCurtaDAO extends GenericDAO<ModeloMensagemCurta> {

	public ModeloMensagemCurtaDAO() {
		super(ModeloMensagemCurta.class);
	}

}