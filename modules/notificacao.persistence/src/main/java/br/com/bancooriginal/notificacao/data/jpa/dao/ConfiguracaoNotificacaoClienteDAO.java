package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.ConfiguracaoNotificacaoCliente;


public class ConfiguracaoNotificacaoClienteDAO extends GenericDAO<ConfiguracaoNotificacaoCliente> {

	public ConfiguracaoNotificacaoClienteDAO() {
		super(ConfiguracaoNotificacaoCliente.class);
	}

}