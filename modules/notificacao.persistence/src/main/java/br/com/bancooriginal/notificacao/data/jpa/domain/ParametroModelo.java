package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the PARAMETRO_MODELO database table.
 * 
 */
@Entity
@Table(name="PARAMETRO_MODELO")
@NamedQuery(name="ParametroModelo.findAll", query="SELECT p FROM ParametroModelo p")
public class ParametroModelo implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ParametroModeloPK id;

	@Column(name="IND_MANDATORIO")
	private String indMandatorio;

	//bi-directional many-to-one association to ModeloMensagem
	@ManyToOne
	@JoinColumn(name="ID_MODELO_MENSAGEM", referencedColumnName="ID_MODELO_MENSAGEM", insertable=false, updatable=false)
	private ModeloMensagem modeloMensagem;

	//bi-directional many-to-many association to ParametroSolicitacao
	@ManyToMany(mappedBy="parametroModelos")
	private List<ParametroSolicitacao> parametroSolicitacaos;

	public ParametroModelo() {
	}

	public ParametroModeloPK getId() {
		return this.id;
	}

	public void setId(ParametroModeloPK id) {
		this.id = id;
	}

	public String getIndMandatorio() {
		return this.indMandatorio;
	}

	public void setIndMandatorio(String indMandatorio) {
		this.indMandatorio = indMandatorio;
	}

	public ModeloMensagem getModeloMensagem() {
		return this.modeloMensagem;
	}

	public void setModeloMensagem(ModeloMensagem modeloMensagem) {
		this.modeloMensagem = modeloMensagem;
	}

	public List<ParametroSolicitacao> getParametroSolicitacaos() {
		return this.parametroSolicitacaos;
	}

	public void setParametroSolicitacaos(List<ParametroSolicitacao> parametroSolicitacaos) {
		this.parametroSolicitacaos = parametroSolicitacaos;
	}

}