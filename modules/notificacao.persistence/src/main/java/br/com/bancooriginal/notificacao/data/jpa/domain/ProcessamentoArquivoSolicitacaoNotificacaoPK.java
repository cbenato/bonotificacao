package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the PROCESSAMENTO_ARQUIVO_SOLICITACAO_NOTIFICACAO database table.
 * 
 */
@Embeddable
public class ProcessamentoArquivoSolicitacaoNotificacaoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="COD_ARQUIVO_SOLICITACAO", insertable=false, updatable=false)
	private long codArquivoSolicitacao;

	@Column(name="COD_PROCESSAMENTO_ARQUIVO")
	private long codProcessamentoArquivo;

	public ProcessamentoArquivoSolicitacaoNotificacaoPK() {
	}
	public long getCodArquivoSolicitacao() {
		return this.codArquivoSolicitacao;
	}
	public void setCodArquivoSolicitacao(long codArquivoSolicitacao) {
		this.codArquivoSolicitacao = codArquivoSolicitacao;
	}
	public long getCodProcessamentoArquivo() {
		return this.codProcessamentoArquivo;
	}
	public void setCodProcessamentoArquivo(long codProcessamentoArquivo) {
		this.codProcessamentoArquivo = codProcessamentoArquivo;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ProcessamentoArquivoSolicitacaoNotificacaoPK)) {
			return false;
		}
		ProcessamentoArquivoSolicitacaoNotificacaoPK castOther = (ProcessamentoArquivoSolicitacaoNotificacaoPK)other;
		return 
			(this.codArquivoSolicitacao == castOther.codArquivoSolicitacao)
			&& (this.codProcessamentoArquivo == castOther.codProcessamentoArquivo);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.codArquivoSolicitacao ^ (this.codArquivoSolicitacao >>> 32)));
		hash = hash * prime + ((int) (this.codProcessamentoArquivo ^ (this.codProcessamentoArquivo >>> 32)));
		
		return hash;
	}
}