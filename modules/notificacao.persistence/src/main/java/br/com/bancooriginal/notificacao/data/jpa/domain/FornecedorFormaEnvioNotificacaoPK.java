package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the FORNECEDOR_FORMA_ENVIO_NOTIFICACAO database table.
 * 
 */
@Embeddable
public class FornecedorFormaEnvioNotificacaoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ID_FORMA_ENVIO_NOTIFICACAO", insertable=false, updatable=false)
	private long idFormaEnvioNotificacao;

	@Column(name="COD_FORNECEDOR", insertable=false, updatable=false)
	private long codFornecedor;

	public FornecedorFormaEnvioNotificacaoPK() {
	}
	public long getIdFormaEnvioNotificacao() {
		return this.idFormaEnvioNotificacao;
	}
	public void setIdFormaEnvioNotificacao(long idFormaEnvioNotificacao) {
		this.idFormaEnvioNotificacao = idFormaEnvioNotificacao;
	}
	public long getCodFornecedor() {
		return this.codFornecedor;
	}
	public void setCodFornecedor(long codFornecedor) {
		this.codFornecedor = codFornecedor;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof FornecedorFormaEnvioNotificacaoPK)) {
			return false;
		}
		FornecedorFormaEnvioNotificacaoPK castOther = (FornecedorFormaEnvioNotificacaoPK)other;
		return 
			(this.idFormaEnvioNotificacao == castOther.idFormaEnvioNotificacao)
			&& (this.codFornecedor == castOther.codFornecedor);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.idFormaEnvioNotificacao ^ (this.idFormaEnvioNotificacao >>> 32)));
		hash = hash * prime + ((int) (this.codFornecedor ^ (this.codFornecedor >>> 32)));
		
		return hash;
	}
}