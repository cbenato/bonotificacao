package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.ConfiguracaoNotificacao;

public class ConfiguracaoNotificacaoDAO extends GenericDAO<ConfiguracaoNotificacao> {

	public ConfiguracaoNotificacaoDAO() {
		super(ConfiguracaoNotificacao.class);
	}

}

