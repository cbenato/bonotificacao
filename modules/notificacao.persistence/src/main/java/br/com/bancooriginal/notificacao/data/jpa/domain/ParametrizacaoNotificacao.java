package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the PARAMETRIZACAO_NOTIFICACAO database table.
 * 
 */
@Entity
@Table(name="PARAMETRIZACAO_NOTIFICACAO")
@NamedQuery(name="ParametrizacaoNotificacao.findAll", query="SELECT p FROM ParametrizacaoNotificacao p")
public class ParametrizacaoNotificacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_PARAMETRIZACAO_NOTIFICACAO")
	private long codParametrizacaoNotificacao;

	@Column(name="COD_SITUACAO")
	private BigDecimal codSituacao;

	@Column(name="DAT_ALTERACAO")
	private Timestamp datAlteracao;

	@Column(name="DAT_INCLUSAO")
	private Timestamp datInclusao;

	@Column(name="IND_COMPLEXIDADE_CONFIGURACAO")
	private String indComplexidadeConfiguracao;

	@Column(name="IND_CONFIGURACAO_DESTINATARIO")
	private String indConfiguracaoDestinatario;

	@Column(name="IND_CONFIGURACAO_PERIODICIDADE")
	private String indConfiguracaoPeriodicidade;

	@Column(name="IND_CONFIGURACAO_VAL_MINIMO")
	private String indConfiguracaoValMinimo;

	@Column(name="IND_LIGADO_PADRAO")
	private String indLigadoPadrao;

	@Column(name="IND_TIPO_DESTINATARIO")
	private String indTipoDestinatario;

	//bi-directional many-to-one association to ConfiguracaoNotificacao
	@OneToMany(mappedBy="parametrizacaoNotificacao")
	private List<ConfiguracaoNotificacao> configuracaoNotificacaos;

	//bi-directional many-to-one association to ParametrizacaoMensagemNotificacao
	@OneToMany(mappedBy="parametrizacaoNotificacao")
	private List<ParametrizacaoMensagemNotificacao> parametrizacaoMensagemNotificacaos;

	//bi-directional many-to-one association to Notificacao
	@ManyToOne
	@JoinColumn(name="COD_NOTIFICACAO")
	private Notificacao notificacao;

	public ParametrizacaoNotificacao() {
	}

	public long getCodParametrizacaoNotificacao() {
		return this.codParametrizacaoNotificacao;
	}

	public void setCodParametrizacaoNotificacao(long codParametrizacaoNotificacao) {
		this.codParametrizacaoNotificacao = codParametrizacaoNotificacao;
	}

	public BigDecimal getCodSituacao() {
		return this.codSituacao;
	}

	public void setCodSituacao(BigDecimal codSituacao) {
		this.codSituacao = codSituacao;
	}

	public Timestamp getDatAlteracao() {
		return this.datAlteracao;
	}

	public void setDatAlteracao(Timestamp datAlteracao) {
		this.datAlteracao = datAlteracao;
	}

	public Timestamp getDatInclusao() {
		return this.datInclusao;
	}

	public void setDatInclusao(Timestamp datInclusao) {
		this.datInclusao = datInclusao;
	}

	public String getIndComplexidadeConfiguracao() {
		return this.indComplexidadeConfiguracao;
	}

	public void setIndComplexidadeConfiguracao(String indComplexidadeConfiguracao) {
		this.indComplexidadeConfiguracao = indComplexidadeConfiguracao;
	}

	public String getIndConfiguracaoDestinatario() {
		return this.indConfiguracaoDestinatario;
	}

	public void setIndConfiguracaoDestinatario(String indConfiguracaoDestinatario) {
		this.indConfiguracaoDestinatario = indConfiguracaoDestinatario;
	}

	public String getIndConfiguracaoPeriodicidade() {
		return this.indConfiguracaoPeriodicidade;
	}

	public void setIndConfiguracaoPeriodicidade(String indConfiguracaoPeriodicidade) {
		this.indConfiguracaoPeriodicidade = indConfiguracaoPeriodicidade;
	}

	public String getIndConfiguracaoValMinimo() {
		return this.indConfiguracaoValMinimo;
	}

	public void setIndConfiguracaoValMinimo(String indConfiguracaoValMinimo) {
		this.indConfiguracaoValMinimo = indConfiguracaoValMinimo;
	}

	public String getIndLigadoPadrao() {
		return this.indLigadoPadrao;
	}

	public void setIndLigadoPadrao(String indLigadoPadrao) {
		this.indLigadoPadrao = indLigadoPadrao;
	}

	public String getIndTipoDestinatario() {
		return this.indTipoDestinatario;
	}

	public void setIndTipoDestinatario(String indTipoDestinatario) {
		this.indTipoDestinatario = indTipoDestinatario;
	}

	public List<ConfiguracaoNotificacao> getConfiguracaoNotificacaos() {
		return this.configuracaoNotificacaos;
	}

	public void setConfiguracaoNotificacaos(List<ConfiguracaoNotificacao> configuracaoNotificacaos) {
		this.configuracaoNotificacaos = configuracaoNotificacaos;
	}

	public ConfiguracaoNotificacao addConfiguracaoNotificacao(ConfiguracaoNotificacao configuracaoNotificacao) {
		getConfiguracaoNotificacaos().add(configuracaoNotificacao);
		configuracaoNotificacao.setParametrizacaoNotificacao(this);

		return configuracaoNotificacao;
	}

	public ConfiguracaoNotificacao removeConfiguracaoNotificacao(ConfiguracaoNotificacao configuracaoNotificacao) {
		getConfiguracaoNotificacaos().remove(configuracaoNotificacao);
		configuracaoNotificacao.setParametrizacaoNotificacao(null);

		return configuracaoNotificacao;
	}

	public List<ParametrizacaoMensagemNotificacao> getParametrizacaoMensagemNotificacaos() {
		return this.parametrizacaoMensagemNotificacaos;
	}

	public void setParametrizacaoMensagemNotificacaos(List<ParametrizacaoMensagemNotificacao> parametrizacaoMensagemNotificacaos) {
		this.parametrizacaoMensagemNotificacaos = parametrizacaoMensagemNotificacaos;
	}

	public ParametrizacaoMensagemNotificacao addParametrizacaoMensagemNotificacao(ParametrizacaoMensagemNotificacao parametrizacaoMensagemNotificacao) {
		getParametrizacaoMensagemNotificacaos().add(parametrizacaoMensagemNotificacao);
		parametrizacaoMensagemNotificacao.setParametrizacaoNotificacao(this);

		return parametrizacaoMensagemNotificacao;
	}

	public ParametrizacaoMensagemNotificacao removeParametrizacaoMensagemNotificacao(ParametrizacaoMensagemNotificacao parametrizacaoMensagemNotificacao) {
		getParametrizacaoMensagemNotificacaos().remove(parametrizacaoMensagemNotificacao);
		parametrizacaoMensagemNotificacao.setParametrizacaoNotificacao(null);

		return parametrizacaoMensagemNotificacao;
	}

	public Notificacao getNotificacao() {
		return this.notificacao;
	}

	public void setNotificacao(Notificacao notificacao) {
		this.notificacao = notificacao;
	}

}