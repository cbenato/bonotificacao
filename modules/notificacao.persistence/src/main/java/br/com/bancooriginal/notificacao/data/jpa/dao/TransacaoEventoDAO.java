package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.TransacaoEvento;

public class TransacaoEventoDAO extends GenericDAO<TransacaoEvento> {

	public TransacaoEventoDAO() {
		super(TransacaoEvento.class);
	}

}