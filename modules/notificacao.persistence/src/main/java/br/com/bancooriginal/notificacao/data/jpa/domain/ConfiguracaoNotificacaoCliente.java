package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the CONFIGURACAO_NOTIFICACAO_CLIENTE database table.
 * 
 */
@Entity
@Table(name="CONFIGURACAO_NOTIFICACAO_CLIENTE")
@NamedQuery(name="ConfiguracaoNotificacaoCliente.findAll", query="SELECT c FROM ConfiguracaoNotificacaoCliente c")
public class ConfiguracaoNotificacaoCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="COD_CONFIGURACAO_NOTIFICACAO")
	private long codConfiguracaoNotificacao;

	@Column(name="COD_PERIODICIDADE")
	private BigDecimal codPeriodicidade;

	@Column(name="COD_PERIODO_ENVIO")
	private BigDecimal codPeriodoEnvio;

	@Column(name="ID_CLIENTE")
	private BigDecimal idCliente;

	@Column(name="ID_RESPONSAVEL")
	private BigDecimal idResponsavel;

	@Column(name="IND_LIGADO")
	private String indLigado;

	@Column(name="VAL_MINIMO")
	private BigDecimal valMinimo;

	public ConfiguracaoNotificacaoCliente() {
	}

	public long getCodConfiguracaoNotificacao() {
		return this.codConfiguracaoNotificacao;
	}

	public void setCodConfiguracaoNotificacao(long codConfiguracaoNotificacao) {
		this.codConfiguracaoNotificacao = codConfiguracaoNotificacao;
	}

	public BigDecimal getCodPeriodicidade() {
		return this.codPeriodicidade;
	}

	public void setCodPeriodicidade(BigDecimal codPeriodicidade) {
		this.codPeriodicidade = codPeriodicidade;
	}

	public BigDecimal getCodPeriodoEnvio() {
		return this.codPeriodoEnvio;
	}

	public void setCodPeriodoEnvio(BigDecimal codPeriodoEnvio) {
		this.codPeriodoEnvio = codPeriodoEnvio;
	}

	public BigDecimal getIdCliente() {
		return this.idCliente;
	}

	public void setIdCliente(BigDecimal idCliente) {
		this.idCliente = idCliente;
	}

	public BigDecimal getIdResponsavel() {
		return this.idResponsavel;
	}

	public void setIdResponsavel(BigDecimal idResponsavel) {
		this.idResponsavel = idResponsavel;
	}

	public String getIndLigado() {
		return this.indLigado;
	}

	public void setIndLigado(String indLigado) {
		this.indLigado = indLigado;
	}

	public BigDecimal getValMinimo() {
		return this.valMinimo;
	}

	public void setValMinimo(BigDecimal valMinimo) {
		this.valMinimo = valMinimo;
	}

}