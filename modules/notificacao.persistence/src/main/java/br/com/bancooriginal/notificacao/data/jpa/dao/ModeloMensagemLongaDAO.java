package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.ModeloMensagemLonga;

public class ModeloMensagemLongaDAO extends GenericDAO<ModeloMensagemLonga> {

	public ModeloMensagemLongaDAO() {
		super(ModeloMensagemLonga.class);
	}

}