package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;


/**
 * The persistent class for the SOLICITACAO_MENSAGEM_NOTIFICACAO database table.
 * 
 */
@Entity
@Table(name="SOLICITACAO_MENSAGEM_NOTIFICACAO")
@NamedQuery(name="SolicitacaoMensagemNotificacao.findAll", query="SELECT s FROM SolicitacaoMensagemNotificacao s")
public class SolicitacaoMensagemNotificacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SolicitacaoMensagemNotificacaoPK id;

	@Column(name="COD_FORMA_ENVIO_NOTIFICACAO")
	private BigDecimal codFormaEnvioNotificacao;

	@Column(name="COD_SITUACAO")
	private BigDecimal codSituacao;

	@Column(name="DAT_AGENDAMENTO_ENVIO")
	private Timestamp datAgendamentoEnvio;

	@Column(name="DAT_ENVIO")
	private Timestamp datEnvio;

	//bi-directional many-to-one association to SituacaoFornecedorMensagem
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="COD_FORNECEDOR", referencedColumnName="COD_FORNECEDOR"),
		@JoinColumn(name="ID_SITUACAO_FORNECEDOR", referencedColumnName="ID_SITUACAO_FORNECEDOR")
		})
	private SituacaoFornecedorMensagem situacaoFornecedorMensagem;

	//bi-directional many-to-one association to SolicitacaoNotificacao
	@ManyToOne
	@JoinColumn(name="COD_SOLICITACAO_NOTIFICACAO",insertable=false, updatable=false)
	private SolicitacaoNotificacao solicitacaoNotificacao;

	public SolicitacaoMensagemNotificacao() {
	}

	public SolicitacaoMensagemNotificacaoPK getId() {
		return this.id;
	}

	public void setId(SolicitacaoMensagemNotificacaoPK id) {
		this.id = id;
	}

	public BigDecimal getCodFormaEnvioNotificacao() {
		return this.codFormaEnvioNotificacao;
	}

	public void setCodFormaEnvioNotificacao(BigDecimal codFormaEnvioNotificacao) {
		this.codFormaEnvioNotificacao = codFormaEnvioNotificacao;
	}

	public BigDecimal getCodSituacao() {
		return this.codSituacao;
	}

	public void setCodSituacao(BigDecimal codSituacao) {
		this.codSituacao = codSituacao;
	}

	public Timestamp getDatAgendamentoEnvio() {
		return this.datAgendamentoEnvio;
	}

	public void setDatAgendamentoEnvio(Timestamp datAgendamentoEnvio) {
		this.datAgendamentoEnvio = datAgendamentoEnvio;
	}

	public Timestamp getDatEnvio() {
		return this.datEnvio;
	}

	public void setDatEnvio(Timestamp datEnvio) {
		this.datEnvio = datEnvio;
	}

	public SituacaoFornecedorMensagem getSituacaoFornecedorMensagem() {
		return this.situacaoFornecedorMensagem;
	}

	public void setSituacaoFornecedorMensagem(SituacaoFornecedorMensagem situacaoFornecedorMensagem) {
		this.situacaoFornecedorMensagem = situacaoFornecedorMensagem;
	}

	public SolicitacaoNotificacao getSolicitacaoNotificacao() {
		return this.solicitacaoNotificacao;
	}

	public void setSolicitacaoNotificacao(SolicitacaoNotificacao solicitacaoNotificacao) {
		this.solicitacaoNotificacao = solicitacaoNotificacao;
	}

}