package br.com.bancooriginal.notificacao.data.jpa.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the FORNECEDOR_FORMA_ENVIO_NOTIFICACAO database table.
 * 
 */
@Entity
@Table(name="FORNECEDOR_FORMA_ENVIO_NOTIFICACAO")
@NamedQuery(name="FornecedorFormaEnvioNotificacao.findAll", query="SELECT f FROM FornecedorFormaEnvioNotificacao f")
public class FornecedorFormaEnvioNotificacao implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private FornecedorFormaEnvioNotificacaoPK id;

	@Column(name="COD_SITUACAO")
	private BigDecimal codSituacao;

	//bi-directional many-to-one association to FormaEnvioNotificacao
	@ManyToOne
	@JoinColumn(name="ID_FORMA_ENVIO_NOTIFICACAO", insertable=false, updatable=false)
	private FormaEnvioNotificacao formaEnvioNotificacao;

	//bi-directional many-to-one association to FornecedorMensagem
	@ManyToOne
	@JoinColumn(name="COD_FORNECEDOR", insertable=false, updatable=false)
	private FornecedorMensagem fornecedorMensagem;

	public FornecedorFormaEnvioNotificacao() {
	}

	public FornecedorFormaEnvioNotificacaoPK getId() {
		return this.id;
	}

	public void setId(FornecedorFormaEnvioNotificacaoPK id) {
		this.id = id;
	}

	public BigDecimal getCodSituacao() {
		return this.codSituacao;
	}

	public void setCodSituacao(BigDecimal codSituacao) {
		this.codSituacao = codSituacao;
	}

	public FormaEnvioNotificacao getFormaEnvioNotificacao() {
		return this.formaEnvioNotificacao;
	}

	public void setFormaEnvioNotificacao(FormaEnvioNotificacao formaEnvioNotificacao) {
		this.formaEnvioNotificacao = formaEnvioNotificacao;
	}

	public FornecedorMensagem getFornecedorMensagem() {
		return this.fornecedorMensagem;
	}

	public void setFornecedorMensagem(FornecedorMensagem fornecedorMensagem) {
		this.fornecedorMensagem = fornecedorMensagem;
	}

}