package br.com.bancooriginal.notificacao.data.jpa.dao;

import br.com.bancooriginal.notificacao.data.jpa.domain.ParametroSolicitacao;

public class ParametroSolicitacaoDAO extends GenericDAO<ParametroSolicitacao> {

	public ParametroSolicitacaoDAO() {
		super(ParametroSolicitacao.class);
	}

}
